from Framework import *



# Dist Match - one touch Enrol and Match using images loaded from PC - PCSC contactless
def one_touch_Enrol_and_Match_using_loaded_images(config):
    if data["Secure element"] == 'THD' or data["Secure element"] == 'SLE':
        PORT = config.port
        INIT_MODE = config.init_mode

        print_info_message("", False, True, False)
        print_info_message('********** Running ' + one_touch_Enrol_and_Match_using_loaded_images.__name__ + ' **********', False, True, False)
        # ENROLL PHASE
        print_info_message("", False, True, False)
        print_info_message("---Enrollment Phase---", False, True, False)
        # Run Power cycle
        auto_or_manual_power_cycle()
        # Run init
        ret = test_tool_cmd(PORT, 'initialize', INIT_MODE)
        if ret != 0:
            return ret
        # Run loadimage test1
        ret = test_tool_cmd(PORT, 'load_image', script_path + "\\" + script_database_dirname + "\\" + script_one_touch_match_loaded_case_dirname + "\\" + script_one_touch_match_loaded_case_images_dirname + "\\" + 'test1.bmp')
        if ret != 0:
            return ret
        # Run enroll
        ret = test_tool_cmd(PORT, 'dm_single_enroll', '2 6 1 1')
        if ret != 0:
            return ret
        # MATCH PHASE
        print_info_message("", False, True, False)
        print_info_message("---Match Phase---", False, True, False)
        # Run Power cycle
        auto_or_manual_power_cycle()
        # Run init
        ret = test_tool_cmd(PORT, 'initialize', INIT_MODE)
        if ret != 0:
            return ret
        # Run loadimage test1
        ret = test_tool_cmd(PORT, 'load_image', script_path + "\\" + script_database_dirname + "\\" + script_one_touch_match_loaded_case_dirname + "\\" + script_one_touch_match_loaded_case_images_dirname + "\\" + 'test1.bmp')
        if ret != 0:
            return ret
        # Run dmmatch loadimage
        ret = test_tool_cmd(PORT, 'dm_match', 'load-img')
        return ret
    elif data["Secure element"] == 'MOCK':
        print_info_message("", False, True, False)
        print_info_message('********** Running ' + one_touch_Enrol_and_Match_using_loaded_images.__name__ + ' **********', False, True, False)
        print_warning_message('********** ' + one_touch_Enrol_and_Match_using_loaded_images.__name__ + ' **********' + " can not be run without THD or SLE element")
        return 1
    else:
        print_info_message("", False, True, False)
        print_info_message('********** Running ' + one_touch_Enrol_and_Match_using_loaded_images.__name__ + ' **********', False, True, False)
        print_warning_message("Secure element name is wrong or not provided")
        return 1


# Dist Match - one touch Enrol and Match using live acquisition - PCSC contactless

def one_touch_Enrol_and_Match_using_live_images(config):

    PORT = config.port
    INIT_MODE = config.init_mode

    print_info_message("", False, True, False)
    print_info_message('********** Running ' + one_touch_Enrol_and_Match_using_live_images.__name__ + ' **********', False, True, False)

    # ENROLL PHASE
    print_info_message("", False, True, False)
    print_info_message("---Enrollment Phase---", False, True, False)
    # Run Power cycle
    auto_or_manual_power_cycle()
    # Run init
    ret = test_tool_cmd(PORT, 'initialize', INIT_MODE)
    if ret != 0:
        return ret
    # Ask user to hold finger on sensor
    print_warning_message("Hold finger on sensor", False, True, False)
    time_out_sec(2)
    # Run acquire finger detect
    ret = test_tool_cmd(PORT, 'acquire', 'fingerdetect')
    if ret != 0:
        return ret
    # Run enroll
    ret = test_tool_cmd(PORT, 'dm_single_enroll', '2 6 1 1')
    if ret != 0:
        return ret
    # MATCH PHASE
    print_info_message("", False, True, False)
    print_info_message("---Match Phase---", False, True, False)
    # Run Power cycle
    auto_or_manual_power_cycle()
    # Run init
    ret = test_tool_cmd(PORT, 'initialize', INIT_MODE)
    if ret != 0:
        return ret
    # Ask user to hold finger on sensor
    print_warning_message("Hold same finger on sensor", False, True, False)
    time_out_sec(2)
    # Run sensor-img match
    ret = test_tool_cmd(PORT, 'dm_match', 'sensor-img')
    if ret != 0:
        return ret
    # Ask user to hold finger on sensor
    print_warning_message("Hold other finger on sensor", False, True, False)
    time_out_sec(2)
    expected_outputs = ['templates do not match']
    ret = test_tool_cmd(PORT, 'dm_match', 'sensor-img', True, expected_outputs)

    return ret


# Dist Match - Enrol and Match 2 fingers 6 touches - PCSC contactless
def enrol_and_Match_2_fingers_6_touches(config):
    PORT = config.port
    INIT_MODE = config.init_mode

    print_info_message("", False, True, False)
    print_info_message('********** Running ' + enrol_and_Match_2_fingers_6_touches.__name__ + ' **********', False, True, False)

    # ENROLL PHASE
    print_info_message("", False, True, False)
    print_info_message("---Enrollment Phase---", False, True, False)
    # Run Power cycle
    auto_or_manual_power_cycle()
    # Run init
    ret = test_tool_cmd(PORT, 'initialize', INIT_MODE)
    if ret != 0:
        return ret
    # Ask user to hold finger on sensor
    print_warning_message("Enroll fingers 1 and 2 following LED sequence", False, True, False)
    # Run dm_enroll
    ret = test_tool_cmd(PORT, 'dm_enroll', '2 6')
    if ret != 0:
        return ret
    # Run Power cycle
    auto_or_manual_power_cycle()

    # MATCH PHASE
    print_info_message("", False, True, False)
    print_info_message("---Match Phase---", False, True, False)
    # Run init
    ret = test_tool_cmd(PORT, 'initialize', INIT_MODE)
    if ret != 0:
        return ret
    # Ask user to hold finger on sensor
    print_warning_message("Hold finger 1 on sensor", False, True, False)
    time_out_sec(2)
    # Run dm_match finger 1
    ret = test_tool_cmd(PORT, 'dm_match', 'sensor-img')
    if ret != 0:
        return ret
    # Ask user to hold finger on sensor
    print_warning_message("Hold finger 2 on sensor", False, True, False)
    time_out_sec(2)
    # Run dm_match finger 2
    ret = test_tool_cmd(PORT, 'dm_match', 'sensor-img')
    if ret != 0:
        return ret
    # Ask user to hold finger on sensor
    print_warning_message("Hold other finger on sensor", False, True, False)
    time_out_sec(2)
    # Run dm_match finger 3
    expected_outputs = ['templates do not match']
    ret = test_tool_cmd(PORT, 'dm_match', 'sensor-img', True, expected_outputs)

    return ret

# Dist Match - Enrol and match over non-secure channel
def enrol_and_match_over_non_secure_channel(config):
    PORT = config.port
    INIT_MODE = config.init_mode

    print_info_message("", False, True, False)
    print_info_message('********** Running ' + enrol_and_match_over_non_secure_channel.__name__ + ' **********', False, True, False)

    # ENROLL PHASE
    print_info_message("", False, True, False)
    print_info_message("---Enrollment Phase---", False, True, False)
    # Run Power cycle
    auto_or_manual_power_cycle()
    # Reset secure channel
    ret = test_tool_cmd(PORT, 'reset', '')
    if ret != 0:
        return ret
    # Run Power cycle
    auto_or_manual_power_cycle()
    # Run init
    ret = test_tool_cmd(PORT, 'initialize', INIT_MODE)
    if ret != 0:
        return ret
    # Ask user to hold finger on sensor
    print_warning_message("Enroll fingers 1 and 2 following LED sequence", False, True, False)
    # Run dm_enroll
    ret = test_tool_cmd(PORT, 'dm_enroll', '2 6')
    if ret != 0:
        return ret
    # MATCH PHASE
    print_info_message("", False, True, False)
    print_info_message("---Match Phase---", False, True, False)
    # Run Power cycle
    auto_or_manual_power_cycle()
    # Run init
    ret = test_tool_cmd(PORT, 'initialize', INIT_MODE)
    if ret != 0:
        return ret
    # Ask user to hold finger on sensor
    print_warning_message("Hold finger 1 on sensor", False, True, False)
    time_out_sec(2)
    # Run dm_match finger 1
    ret = test_tool_cmd(PORT, 'dm_match', 'sensor-img')
    if ret != 0:
        return ret
    # Ask user to hold finger on sensor
    print_warning_message("Hold finger 2 on sensor", False, True, False)
    time_out_sec(2)
    # Run dm_match finger 2
    ret = test_tool_cmd(PORT, 'dm_match', 'sensor-img')
    if ret != 0:
        return ret
    # Ask user to hold finger on sensor
    print_warning_message("Hold other finger on sensor", False, True, False)
    time_out_sec(2)
    # Run dm_match finger 3
    expected_outputs = ['templates do not match']
    ret = test_tool_cmd(PORT, 'dm_match', 'sensor-img', True, expected_outputs)

    return ret





# Template management SE testing using enrol pattern - PCSC Contactless
def enrol_pattern(config):
    PORT = config.port
    INIT_MODE = config.init_mode

    print_info_message("", False, True, False)
    print_info_message('********** Running ' + enrol_pattern.__name__ + ' **********', False, True, False)
    # ENROLL PHASE
    print_info_message("", False, True, False)
    print_info_message("---Enrollment Phase---", False, True, False)
    # Run Power cycle
    auto_or_manual_power_cycle()
    # Run init
    ret = test_tool_cmd(PORT, 'initialize', INIT_MODE)
    if ret != 0:
        return ret
    # Ask user to hold finger on sensor
    print_warning_message("Enrol pattern F1, F2, F3 and F4 following LED sequence", False, True, False)
    # Run dm_enroll
    ret = test_tool_cmd(PORT, 'dm_enroll', '2 2')
    if ret != 0:
        return ret
    # MATCH PHASE
    print_info_message("", False, True, False)
    print_info_message("---Match Phase---", False, True, False)
    # Ask user to hold finger on sensor
    print_warning_message("Hold finger 1 on sensor", False, True, False)
    time_out_sec(2)
    # Run dm_match finger 1
    expected_outputs = ['Total match time', 'Match result: F1T1']
    ret = test_tool_cmd(PORT, 'dm_match timestamp', 'sensor-img', False, expected_outputs)
    if ret != 0:
        return ret
    # Ask user to hold finger on sensor
    print_warning_message("Hold finger 2 on sensor", False, True, False)
    time_out_sec(2)
    # Run dm_match finger 2
    expected_outputs = ['Total match time', 'Match result: F1T2']
    ret = test_tool_cmd(PORT, 'dm_match timestamp', 'sensor-img', False, expected_outputs)
    if ret != 0:
        return ret
    # Ask user to hold finger on sensor
    print_warning_message("Hold finger 3 on sensor", False, True, False)
    time_out_sec(2)
    # Run dm_match finger 3
    expected_outputs = ['Total match time', 'Match result: F2T1']
    ret = test_tool_cmd(PORT, 'dm_match timestamp', 'sensor-img', False, expected_outputs)
    if ret != 0:
        return ret
    # Ask user to hold finger on sensor
    print_warning_message("Hold finger 4 on sensor", False, True, False)
    time_out_sec(2)
    # Run dm_match finger 4
    expected_outputs = ['Total match time', 'Match result: F2T2']
    ret = test_tool_cmd(PORT, 'dm_match timestamp', 'sensor-img', False, expected_outputs)
    if ret != 0:
        return ret
    # Ask user to hold finger on sensor
    print_warning_message("Hold non-enrolled finger on sensor", False, True, False)
    time_out_sec(2)
    # Run dm_match non-enrolled finger
    expected_outputs = ['Total match time', 'Match result: no match']
    ret = test_tool_cmd(PORT, 'dm_match timestamp', 'sensor-img', True, expected_outputs)
    return ret

# Dist Match - Delete enrolled fingers - PCSC contactless
def delete_enrolled_fingers(config):
    PORT = config.port
    INIT_MODE = config.init_mode

    print_info_message("", False, True, False)
    print_info_message('********** Running ' + delete_enrolled_fingers.__name__ + ' **********', False, True, False)
    # ENROLL PHASE
    print_info_message("", False, True, False)
    print_info_message("---Enrollment Phase---", False, True, False)
    # Run Power cycle
    auto_or_manual_power_cycle()
    # Run init
    ret = test_tool_cmd(PORT, 'initialize', INIT_MODE)
    if ret != 0:
        return ret
    # Ask user to hold finger on sensor
    print_warning_message("Enroll fingers 1 and 2 following LED sequence", False, True, False)
    # Run dm_enroll
    ret = test_tool_cmd(PORT, 'dm_enroll', '2 6')
    if ret != 0:
        return ret
    # MATCH PHASE
    print_info_message("", False, True, False)
    print_info_message("---Match Phase---", False, True, False)
    # Run Power cycle
    auto_or_manual_power_cycle()
    # Run init
    ret = test_tool_cmd(PORT, 'initialize', INIT_MODE)
    if ret != 0:
        return ret
    # Run list
    expected_outputs = ['Number of records : 12']
    ret = test_tool_cmd(PORT, 'list', '-1', False, expected_outputs)
    if ret != 0:
        return ret
    # Ask user to hold finger on sensor
    print_warning_message("Hold finger 1 on sensor", False, True, False)
    time_out_sec(2)
    # Run dm_match finger 1
    ret = test_tool_cmd(PORT, 'dm_match', 'sensor-img')
    if ret != 0:
        return ret
    # Ask user to hold finger on sensor
    print_warning_message("Hold finger 2 on sensor", False, True, False)
    time_out_sec(2)
    # Run dm_match finger 2
    ret = test_tool_cmd(PORT, 'dm_match', 'sensor-img')
    if ret != 0:
        return ret
    # Run delete finger
    ret = test_tool_cmd(PORT, 'dm_delete_finger', '1 6')
    if ret != 0:
        return ret
    # Run list
    expected_outputs = ['Number of records : 6']
    ret = test_tool_cmd(PORT, 'list', '-1', False, expected_outputs)
    if ret != 0:
        return ret
    # Ask user to hold finger on sensor
    print_warning_message("Hold finger 1 on sensor", False, True, False)
    time_out_sec(2)
    # Run dm_match finger 1
    expected_outputs = ['templates do not match']
    ret = test_tool_cmd(PORT, 'dm_match', 'sensor-img', True, expected_outputs)
    if ret != 0:
        return ret
    # Ask user to hold finger on sensor
    print_warning_message("Hold finger 2 on sensor", False, True, False)
    time_out_sec(2)
    # Run dm_match finger 2
    ret = test_tool_cmd(PORT, 'dm_match', 'sensor-img')
    if ret != 0:
        return ret
    # Run delete finger
    ret = test_tool_cmd(PORT, 'dm_delete_finger', '2 6')
    if ret != 0:
        return ret
    # Run list
    expected_outputs = ['Record not found']
    ret = test_tool_cmd(PORT, 'list', '-1', True, expected_outputs)
    if ret != 0:
        return ret
    # Ask user to hold finger on sensor
    print_warning_message("Hold finger 2 on sensor", False, True, False)
    time_out_sec(2)
    # Run dm_match finger 2
    expected_outputs = ['Record not found']
    ret = test_tool_cmd(PORT, 'dm_match', 'sensor-img', True, expected_outputs)
    return ret

# Get and Set parameters - PCSC contactless
def get_and_set_parameters(config):
    PORT = config.port
    INIT_MODE = config.init_mode

    print_info_message("", False, True, False)
    print_info_message('********** Running ' + get_and_set_parameters.__name__ + ' **********', False, True, False)
    # Run Power cycle
    auto_or_manual_power_cycle()
    # Run init
    ret = test_tool_cmd(PORT, 'initialize', INIT_MODE)
    if ret != 0:
        return ret
    # Run get param 200
    expected_outputs = ['param[200] = 1000']
    ret = test_tool_cmd(PORT, 'get_param', '200', False, expected_outputs)
    if ret != 0:
        return ret
    # Run set param 200 750
    ret = test_tool_cmd(PORT, 'set_param 200', '750')
    if ret != 0:
        return ret
    # Run get param 200
    expected_outputs = ['param[200] = 750']
    ret = test_tool_cmd(PORT, 'get_param', '200', False, expected_outputs)
    return ret



# Timestamp - exercising dm_match timestamp functionality - PCSC Contactless
def dm_match_timestamp(config):
    PORT = config.port
    INIT_MODE = config.init_mode

    print_info_message("", False, True, False)
    print_info_message('********** Running ' + dm_match_timestamp.__name__ + ' **********', False, True, False)
    # ENROLL PHASE
    print_info_message("", False, True, False)
    print_info_message("---Enrollment Phase---", False, True, False)
    # Run Power cycle
    auto_or_manual_power_cycle()
    # Run init
    ret = test_tool_cmd(PORT, 'initialize', INIT_MODE)
    if ret != 0:
        return ret
    # Ask user to hold finger on sensor
    print_warning_message("Hold finger 1 on sensor", False, True, False)
    # Run acquire fingerdetect
    ret = test_tool_cmd(PORT, 'acquire', 'fingerdetect')
    if ret != 0:
        return ret
    # Run enroll
    ret = test_tool_cmd(PORT, 'dm_single_enroll', '2 6 1 1')
    if ret != 0:
        return ret
    # MATCH PHASE
    print_info_message("", False, True, False)
    print_info_message("---Match Phase---", False, True, False)
    # Run Power cycle
    auto_or_manual_power_cycle()
    # Run init
    ret = test_tool_cmd(PORT, 'initialize', INIT_MODE)
    if ret != 0:
        return ret
    # Ask user to hold finger on sensor
    print_warning_message("Hold finger 1 on sensor", False, True, False)
    time_out_sec(2)
    # Run dm_match finger 1
    expected_outputs = ['Total match time', 'Match result: F1T1']
    ret = test_tool_cmd(PORT, 'dm_match timestamp', 'sensor-img', False, expected_outputs)
    if ret != 0:
        return ret
    # Ask user to hold finger on sensor
    print_warning_message("Hold other finger on sensor", False, True, False)
    time_out_sec(2)
    # Run dm_match non-enrolled finger
    expected_outputs = ['Total match time', 'Match result: no match']
    ret = test_tool_cmd(PORT, 'dm_match timestamp', 'sensor-img', True, expected_outputs)
    return ret

# MWF (matcher work function) - PCSC Contactless
def mwf_function(config):
    PORT = config.port
    INIT_MODE = config.init_mode

    print_info_message("", False, True, False)
    print_info_message('********** Running ' + mwf_function.__name__ + ' **********', False, True, False)
    # ENROLL PHASE
    print_info_message("", False, True, False)
    print_info_message("---Enrollment Phase---", False, True, False)
    # Run Power cycle
    auto_or_manual_power_cycle()
    # Run init
    ret = test_tool_cmd(PORT, 'initialize', INIT_MODE)
    if ret != 0:
        return ret
    # Ask user to hold finger on sensor
    print_warning_message("Hold finger 1 on sensor", False, True, False)
    # Run acquire fingerdetect
    ret = test_tool_cmd(PORT, 'acquire', 'fingerdetect')
    if ret != 0:
        return ret
    # Run enroll
    ret = test_tool_cmd(PORT, 'dm_single_enroll', '2 6 1 1')
    if ret != 0:
        return ret
    # MATCH PHASE
    print_info_message("", False, True, False)
    print_info_message("---Match Phase---", False, True, False)
    # Run Power cycle
    auto_or_manual_power_cycle()
    # Run init
    ret = test_tool_cmd(PORT, 'initialize', INIT_MODE)
    if ret != 0:
        return ret
    # Ask user to hold finger on sensor
    print_warning_message("Hold same finger on sensor", False, True, False)
    time_out_sec(2)
    # Run dmmatch loadimage
    ret = test_tool_cmd(PORT, 'dm_match timestamp', 'sensor-img', 'mwf7902')
    if ret != 0:
        return ret
    # Ask user to hold finger on sensor
    print_warning_message("Hold same finger on sensor", False, True, False)
    time_out_sec(2)
    ret = test_tool_cmd(PORT, 'dm_match timestamp', 'sensor-img', 'mwf3500')
    if ret != 0:
        return ret
    # Ask user to hold finger on sensor
    print_warning_message("Hold same finger on sensor", False, True, False)
    time_out_sec(2)
    ret = test_tool_cmd(PORT, 'dm_match timestamp', 'sensor-img', 'mwf1000')
    if ret != 0:
        return ret
    # Ask user to hold finger on sensor
    print_warning_message("Hold same finger on sensor", False, True, False)
    time_out_sec(2)
    ret = test_tool_cmd(PORT, 'dm_match timestamp', 'sensor-img', 'mwf100')
    return ret

# SE version reporting - PCSC Contactless
def version_reporting(config):
    PORT = config.port
    INIT_MODE = config.init_mode

    print_info_message("", False, True, False)
    print_info_message('********** Running ' + version_reporting.__name__ + ' **********', False, True, False)
    # Run Power cycle
    auto_or_manual_power_cycle()
    # Run init
    ret = test_tool_cmd(PORT, 'initialize', INIT_MODE)
    if ret != 0:
        return ret
    # Run get_version
    expected_outputs = [data['SE version'], data['MCU version'], data['Matcher version'], data['FW version'], data['Sensor info']]
    ret = test_tool_cmd(PORT, 'get_version', '', False, expected_outputs)
    return ret

# Secure channel, SE enable
def secure_channel_SE_enable(config):
    if data["Secure element"] == 'THD':
        PORT = config.port
        INIT_MODE = config.init_mode

        print_info_message("", False, True, False)
        print_info_message('********** Running ' + secure_channel_SE_enable.__name__ + ' **********', False, True, False)
        # Run Power cycle
        auto_or_manual_power_cycle()
        # Reset secure channel
        ret = test_tool_cmd(PORT, 'reset', '')
        if ret != 0:
            return ret
        # Run Power cycle
        auto_or_manual_power_cycle()
        # Run set_factory_keys
        ret = test_tool_cmd(PORT, 'set_factory_keys', '')
        # Run Power cycle
        auto_or_manual_power_cycle()
        return ret
    elif data["Secure element"] == 'MOCK' or data["Secure element"] == 'SLE':
        print_info_message("", False, True, False)
        print_info_message('********** Running ' + secure_channel_SE_enable.__name__ + ' **********', False, True, False)
        print_warning_message('********** ' + secure_channel_SE_enable.__name__ + ' **********' + " can not be run without THD element")
        return 1
    else:
        print_info_message("", False, True, False)
        print_info_message('********** Running ' + secure_channel_SE_enable.__name__ + ' **********', False, True, False)
        print_warning_message("Secure element name is wrong or not provided")
        return 1


# Dist Match - Enrol and Match over secure channel
def enrol_and_match_over_secure_channel(config):
    if data["Secure element"] == 'THD':
        PORT = config.port
        INIT_MODE = config.init_mode

        print_info_message("", False, True, False)
        print_info_message('********** Running ' + enrol_and_match_over_secure_channel.__name__ + ' **********', False, True, False)
        # ENROLL PHASE
        print_info_message("", False, True, False)
        print_info_message("---Enrollment Phase---", False, True, False)
        # Run Power cycle
        auto_or_manual_power_cycle()
        # Run init
        ret = test_tool_cmd(PORT, 'initialize', INIT_MODE)
        if ret != 0:
            return ret
        # Run se_scp
        ret = test_tool_cmd(PORT, 'se_scp', '')
        if ret != 0:
            return ret
        # Ask user to hold finger on sensor
        print_warning_message("Enroll fingers 1 and 2 following LED sequence", False, True, False)
        # Run dm_enroll
        ret = test_tool_cmd(PORT, 'dm_enroll', '2 6')
        if ret != 0:
            return ret
        # MATCH PHASE
        print_info_message("", False, True, False)
        print_info_message("---Match Phase---", False, True, False)
        auto_or_manual_power_cycle()
        # Run init
        ret = test_tool_cmd(PORT, 'initialize', INIT_MODE)
        if ret != 0:
            return ret
        # Run se_scp
        ret = test_tool_cmd(PORT, 'se_scp', '')
        if ret != 0:
            return ret
        # Ask user to hold finger on sensor
        print_warning_message("Hold finger 1 on sensor", False, True, False)
        time_out_sec(2)
        # Run dm_match finger 1
        ret = test_tool_cmd(PORT, 'dm_match', '')
        if ret != 0:
            return ret
        # Ask user to hold finger on sensor
        print_warning_message("Hold finger 2 on sensor", False, True, False)
        time_out_sec(2)
        # Run dm_match finger 2
        ret = test_tool_cmd(PORT, 'dm_match', '')
        if ret != 0:
            return ret
        # Ask user to hold finger on sensor
        print_warning_message("Hold other finger on sensor", False, True, False)
        time_out_sec(2)
        # Run dm_match finger 3
        expected_outputs = ['templates do not match']
        ret = test_tool_cmd(PORT, 'dm_match', '', True, expected_outputs)
        return ret
    elif data["Secure element"] == 'MOCK' or data["Secure element"] == 'SLE':
        print_info_message("", False, True, False)
        print_info_message('********** Running ' + enrol_and_match_over_secure_channel.__name__ + ' **********', False, True, False)
        print_warning_message('********** ' + enrol_and_match_over_secure_channel.__name__ + ' **********' + " can not be run without THD element")
        return 1
    else:
        print_info_message("", False, True, False)
        print_info_message('********** Running ' + enrol_and_match_over_secure_channel.__name__ + ' **********', False, True, False)
        print_warning_message("Secure element name is wrong or not provided")
        return 1

# Get and Set parameters over secure channel - PCSC contactless
def get_and_set_parameters_over_secure_channel(config):
    if data["Secure element"] == 'THD':
        PORT = config.port
        INIT_MODE = config.init_mode

        print_info_message("", False, True, False)
        print_info_message('********** Running ' + get_and_set_parameters_over_secure_channel.__name__ + ' **********', False, True, False)
        # Run Power cycle
        auto_or_manual_power_cycle()
        # Run init
        ret = test_tool_cmd(PORT, 'initialize', INIT_MODE)
        if ret != 0:
            return ret
        # Run se_scp
        ret = test_tool_cmd(PORT, 'se_scp', '')
        if ret != 0:
            return ret
        # Run get param 200
        expected_outputs = ['param[200] = 1000']
        ret = test_tool_cmd(PORT, 'get_param', '200', False, expected_outputs)
        if ret != 0:
            return ret
        # Run set param 200 100
        ret = test_tool_cmd(PORT, 'set_param 200', '100')
        if ret != 0:
            return ret
        # Run get param 200
        expected_outputs = ['param[200] = 100']
        ret = test_tool_cmd(PORT, 'get_param', '200', False, expected_outputs)
        if ret != 0:
            return ret
        # Run store params
        ret = test_tool_cmd(PORT, 'store_params', '')
        if ret != 0:
            return ret
        # Run Power cycle
        auto_or_manual_power_cycle()
        # Run init
        ret = test_tool_cmd(PORT, 'initialize', INIT_MODE)
        if ret != 0:
            return ret
        # Run se_scp
        ret = test_tool_cmd(PORT, 'se_scp', '')
        if ret != 0:
            return ret
        # Run get param 200
        expected_outputs = ['param[200] = 100']
        ret = test_tool_cmd(PORT, 'get_param', '200', False, expected_outputs)
        if ret != 0:
            return ret
        # Run Power cycle
        auto_or_manual_power_cycle()
        # Run reset
        ret = test_tool_cmd(PORT, 'reset', '')
        return ret
    elif data["Secure element"] == 'MOCK' or data["Secure element"] == 'SLE':
        print_info_message("", False, True, False)
        print_info_message('********** Running ' + get_and_set_parameters_over_secure_channel.__name__ + ' **********', False, True, False)
        print_warning_message('********** ' + get_and_set_parameters_over_secure_channel.__name__ + ' **********' + " can not be run without THD element")
        return 1
    else:
        print_info_message("", False, True, False)
        print_info_message('********** Running ' + get_and_set_parameters_over_secure_channel.__name__ + ' **********', False, True, False)
        print_warning_message("Secure element name is wrong or not provided")
        return 1
