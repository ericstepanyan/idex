from Framework import *




# Run 4patterns_case brief description
# Load images from 4patterns_case DB
# dm-single_enroll using 2 2
# List records
# Load images
# dm_match using same sequence
# dm_match expecting bad quality
# dm_match expecting negative case
def run_4patterns_case(config):
    PORT = config.port
    INIT_MODE = config.init_mode
    ret = 1
    print_info_message("", False, True, False)
    print_info_message('********** Running ' + run_4patterns_case.__name__ + ' **********', False, True, False)

    # ENROLL PHASE
    print_info_message("", False, True, False)
    print_info_message("---Enrollment Phase---", False, True, False)
    # Run Power cycle
    auto_or_manual_power_cycle()
    # Run init
    ret = test_tool_cmd(PORT, 'initialize', INIT_MODE)
    if ret != 0:
        return ret
    for i in range(1, 3):
        for j in range(1, 3):
            ret = test_tool_cmd(PORT, 'load_image',
                                script_path + "\\" + script_database_dirname + "\\" + script_4patterns_case_dirname + "\\" + script_4patterns_case_images_dirname + "\\" + str(
                                    ((i - 1) * 2) + j) + '.bmp')
            if ret != 0:
                return ret
            ret = test_tool_cmd(PORT, 'dm_single_enroll', '2 2 ' + str(i) + ' ' + str(j), False)
            if ret != 0:
                return ret
    ret = test_tool_cmd(PORT, 'list', '-1')
    if ret != 0:
        return ret

    # MATCH PHASE
    print_info_message("", False, True, False)
    print_info_message("---Match Phase---", False, True, False)
    # Run Power cycle
    auto_or_manual_power_cycle()
    # Run init
    ret = test_tool_cmd(PORT, 'initialize', INIT_MODE)
    if ret != 0:
        return ret
    for i in range(1, 3):
        for j in range(1, 3):
            ret = test_tool_cmd(PORT, 'load_image',
                                script_path + "\\" + script_database_dirname + "\\" + script_4patterns_case_dirname + "\\" + script_4patterns_case_images_dirname + "\\" + str(
                                    ((i - 1) * 2) + j) + '.bmp')
            if ret != 0:
                return ret
            else:
                expected_outputs = ["Match result: F" + str(i) + "T" + str(j), 'result - 0x2f']  # list of strings
                ret = test_tool_cmd(PORT, 'dm_match', 'timestamp load-img', False, expected_outputs)
                if ret != 0:
                    return ret
    ret = test_tool_cmd(PORT, 'load_image',
                        script_path + "\\" + script_database_dirname + "\\" + script_4patterns_case_dirname + "\\" + script_4patterns_case_images_dirname + "\\" + 'imposter.bmp')
    if ret != 0:
        return ret
    expected_outputs = ['device return code=0x6747 quality is too bad']
    ret = test_tool_cmd(PORT, 'dm_match', 'timestamp load-img', True, expected_outputs)
    if ret != 0:
        return ret
    ret = test_tool_cmd(PORT, 'load_image',
                        script_path + "\\" + script_database_dirname + "\\" + script_4patterns_case_dirname + "\\" + script_4patterns_case_images_dirname + "\\" + 'no_match_1.bmp')
    if ret != 0:
        return ret
    expected_outputs = ['Match result: no match', 'result - 0x00']
    ret = test_tool_cmd(PORT, 'dm_match', 'timestamp load-img', True, expected_outputs)
    if ret != 0:
        return ret
    ret = test_tool_cmd(PORT, 'load_image',
                        script_path + "\\" + script_database_dirname + "\\" + script_4patterns_case_dirname + "\\" + script_4patterns_case_images_dirname + "\\" + 'no_match_2.bmp')
    if ret != 0:
        return ret
    expected_outputs = ['Match result: no match', 'result - 0x00']
    ret = test_tool_cmd(PORT, 'dm_match', 'timestamp load-img', True, expected_outputs)
    if ret != 0:
        return ret
    return ret


# Run Timestamp_case  Enrol and match (Dist Match) over non-secure channel case
# Brief description:
# Ask user to hold finger on sensor till the end of test case
# Acquire live and dm_single_enroll using 2 6
# Match with timestamp sensor-img and match without timestamp sensor-img

def run_time_stamp_case(config):
    PORT = config.port
    INIT_MODE = config.init_mode

    print_info_message("", False, True, False)
    print_info_message('********** Running ' + run_time_stamp_case.__name__ + ' **********', False, True, False)
    # ENROLL PHASE
    print_info_message("", False, True, False)
    print_info_message("---Enrollment Phase---", False, True, False)
    # Run Power cycle
    auto_or_manual_power_cycle()
    # Run init
    ret = test_tool_cmd(PORT, 'initialize', INIT_MODE)
    if ret != 0:
        return ret
    # Run enroll using live images
    for i in range(1, 7):
        # Ask user to hold finger on sensor
        print_warning_message("Hold first finger on sensor", False, True, False)
        time_out_sec(2)
        ret = test_tool_cmd(PORT, 'acquire', 'live')
        if ret != 0:
            return ret
        ret = test_tool_cmd(PORT, 'dm_single_enroll', '2 6 1 ' + str(i))
        if ret != 0:
            return ret
    for i in range(1, 7):
        # Ask user to hold finger on sensor
        print_warning_message("Hold second finger on sensor", False, True, False)
        time_out_sec(2)
        ret = test_tool_cmd(PORT, 'acquire', 'live')
        if ret != 0:
            return ret
        ret = test_tool_cmd(PORT, 'dm_single_enroll', '2 6 2 ' + str(i))
        if ret != 0:
            return ret
    # MATCH PHASE
    print_info_message("", False, True, False)
    print_info_message("---Match Phase---", False, True, False)
    # Run Power cycle
    auto_or_manual_power_cycle()
    ret = test_tool_cmd(PORT, 'initialize', INIT_MODE)
    if ret != 0:
        return ret
    # Ask user to hold finger on sensor
    print_warning_message("Hold first finger on sensor", False, True, False)
    time_out_sec(2)
    expected_outputs = ['Total match time:']
    ret = test_tool_cmd(PORT, 'dm_match', 'timestamp sensor-img', False, expected_outputs)
    if ret != 0:
        return ret
    # Ask user to hold finger on sensor
    print_warning_message("Hold first finger on sensor", False, True, False)
    time_out_sec(2)
    ret = test_tool_cmd(PORT, 'dm_match', '')
    return ret


# Run Enrol and match (Dist Match) over non-secure channel case
# def run_enrol_match_case(config):
#     PORT = config.port
#     INIT_MODE = config.init_mode
#
#     # Run Power cycle
#     auto_or_manual_power_cycle()
#     # Reset secure channel
#     ret = test_tool_cmd(PORT, 'reset', '', )
#     if ret != 0:
#         return ret
#     # Run Power cycle
#     auto_or_manual_power_cycle()
#     # ENROLL PHASE
#     ret = test_tool_cmd(PORT, 'initialize', INIT_MODE)
#     if ret != 0:
#         return ret
#     # Run enroll using live images
#     for i in range(1, 7):
#         ret = test_tool_cmd(PORT, 'acquire', 'live', )
#         if ret != 0:
#             return ret
#         ret = test_tool_cmd(PORT, 'dm_single_enroll', '2 6 1' + str(i))
#         if ret != 0:
#             return ret
#         ret = test_tool_cmd(PORT, 'acquire', 'live', )
#         if ret != 0:
#             return ret
#         ret = test_tool_cmd(PORT, 'dm_single_enroll', '2 6 2' + str(i))
#         if ret != 0:
#             return ret
#     # Run Power cycle
#     auto_or_manual_power_cycle()
#     # MATCH PHASE
#     ret = test_tool_cmd(PORT, 'initialize', INIT_MODE)
#     if ret != 0:
#         return ret
#     ret = test_tool_cmd(PORT, 'dm_match', '')
#     if ret != 0:
#         return ret
#     ret = test_tool_cmd(PORT, 'dm_match', '')
#     if ret != 0:
#         return ret
#     ret = test_tool_cmd(PORT, 'dm_match', '')
#     if ret != 0:
#         return ret

#  Run Enrol & MATCH (IDM) over secure channel case
# def run_enrol_match_SC_case(config):
#     PORT = config.port
#     INIT_MODE = config.init_mode
#
#     print_info_message("", False, True, False)
#     print_info_message('********** Running ' + run_enrol_match_SC_case.__name__ + ' **********', False, True, False)
#
#     # Run Power cycle
#     auto_or_manual_power_cycle()
#     # Reset secure channel
#     ret = test_tool_cmd(PORT, 'reset', '')
#     if ret != 0:
#         return ret
#     # Set factory keys
#     ret = test_tool_cmd(PORT, 'set_factory_keys', '')
#     if ret != 0:
#         return ret
#     # Run Power cycle
#     auto_or_manual_power_cycle()
#     # Ask user to hold finger on sensor
#     print_warning_message("Hold first finger on sensor", False, True, False)
#     time_out_sec(2)
#     # Run init
#     ret = test_tool_cmd(PORT, 'initialize', INIT_MODE)
#     if ret != 0:
#         return ret
#     # Establish secure channel
#     ret = test_tool_cmd(PORT, 'se_scp', '')
#     if ret != 0:
#         return ret
#     # ENROLL PHASE
#     for i in range(1, 7):
#         # Ask user to hold finger on sensor
#         print_warning_message("Hold first finger on sensor", False, True, False)
#         time_out_sec(2)
#         ret = test_tool_cmd(PORT, 'acquire', 'live', )
#         if ret != 0:
#             return ret
#         ret = test_tool_cmd(PORT, 'dm_single_enroll', '2 6 1' + str(i))
#         if ret != 0:
#             return ret
#     for i in range(1, 7):
#         # Ask user to hold finger on sensor
#         print_warning_message("Hold first finger on sensor", False, True, False)
#         time_out_sec(2)
#         ret = test_tool_cmd(PORT, 'acquire', 'live', )
#         if ret != 0:
#             return ret
#         ret = test_tool_cmd(PORT, 'dm_single_enroll', '2 6 2' + str(i))
#         if ret != 0:
#             return ret
#         # Run Power cycle
#         auto_or_manual_power_cycle()
#     # Run init
#     ret = test_tool_cmd(PORT, 'initialize', INIT_MODE)
#     if ret != 0:
#         return ret
#     # Establish secure channel
#     ret = test_tool_cmd(PORT, 'se_scp', '')
#     if ret != 0:
#         return ret
#     # MATCH PHASE
#     # Ask user to hold finger on sensor
#     print_warning_message("Hold first finger on sensor", False, True, False)
#     time_out_sec(2)
#     ret = test_tool_cmd(PORT, 'dm_match', '')
#     if ret != 0:
#         return ret
#     # Ask user to hold finger on sensor
#     print_warning_message("Hold first finger on sensor", False, True, False)
#     time_out_sec(2)
#     ret = test_tool_cmd(PORT, 'dm_match', '')
#     if ret != 0:
#         return ret
#     # Ask user to hold finger on sensor
#     print_warning_message("Hold first finger on sensor", False, True, False)
#     time_out_sec(2)
#     ret = test_tool_cmd(PORT, 'dm_match', '')
#
#     return ret

# Run DELETE_FINGER case
# Acquire live and enroll 2 6 1 1-6 and 2 6 2 1-6
# List 64
# dm_match first finger
# Delete finger 1 6
# Ensure that first finger doesnt match
# dm_match second finger
# Delete finger 2 6
# List 64
# Ensure that second finger doesnt match
def run_delete_finger_case(config):
    PORT = config.port
    INIT_MODE = config.init_mode

    print_info_message("", False, True, False)
    print_info_message('********** Running ' + run_delete_finger_case.__name__ + ' **********', False, True, False)
    # ENROLL PHASE
    print_info_message("", False, True, False)
    print_info_message("---Enrollment Phase---", False, True, False)
    # Run Power cycle
    auto_or_manual_power_cycle()
    # Run init
    ret = test_tool_cmd(PORT, 'initialize', INIT_MODE)
    if ret != 0:
        return ret

    # ENROLL PHASE
    for i in range(1, 7):
        # Ask user to hold finger on sensor
        print_warning_message("Hold first finger on sensor", False, True, False)
        time_out_sec(2)
        ret = test_tool_cmd(PORT, 'acquire', 'live', )
        if ret != 0:
            return ret
        ret = test_tool_cmd(PORT, 'dm_single_enroll', ' 2 6 1 ' + str(i))
        if ret != 0:
            return ret
    for i in range(1, 7):
        # Ask user to hold finger on sensor
        print_warning_message("Hold second finger on sensor", False, True, False)
        time_out_sec(2)
        ret = test_tool_cmd(PORT, 'acquire', 'live', )
        if ret != 0:
            return ret
        ret = test_tool_cmd(PORT, 'dm_single_enroll', ' 2 6 2 ' + str(i))
        if ret != 0:
            return ret
    # MATCH PHASE
    print_info_message("", False, True, False)
    print_info_message("---Match Phase---", False, True, False)
    # Run Power cycle
    auto_or_manual_power_cycle()
    # Run init
    ret = test_tool_cmd(PORT, 'initialize', INIT_MODE)
    if ret != 0:
        return ret
    # Run list
    ret = test_tool_cmd(PORT, 'list', '64')
    if ret != 0:
        return ret
    # Ask user to hold finger on sensor
    print_warning_message("Hold first finger on sensor", False, True, False)
    time_out_sec(2)
    # Run Match
    ret = test_tool_cmd(PORT, 'dm_match', 'sensor-img')
    if ret != 0:
        return ret
    # Run delete finger
    ret = test_tool_cmd(PORT, 'dm_delete_finger', '1 6')
    if ret != 0:
        return ret
    # Ask user to hold finger on sensor
    print_warning_message("Hold first finger on sensor", False, True, False)
    time_out_sec(2)
    # Run Match
    expected_outputs = ['templates do not match', 'result - 0x00']
    ret = test_tool_cmd(PORT, 'dm_match', 'sensor-img', True, expected_outputs)
    if ret != 0:
        return ret
    # Ask user to hold finger on sensor
    print_warning_message("Hold second finger on sensor", False, True, False)
    time_out_sec(2)
    # Run Match
    ret = test_tool_cmd(PORT, 'dm_match', 'sensor-img')
    if ret != 0:
        return ret
    # Run delete finger
    ret = test_tool_cmd(PORT, 'dm_delete_finger', '2 6')
    if ret != 0:
        return ret
    # Run list
    expected_outputs = ['Record not found']
    ret = test_tool_cmd(PORT, 'list', '64', True, expected_outputs)
    if ret != 0:
        return ret
    # Ask user to hold finger on sensor
    print_warning_message("Hold second finger on sensor", False, True, False)
    time_out_sec(2)
    # Run Match
    expected_outputs = ['Record not found']
    ret = test_tool_cmd(PORT, 'dm_match', 'sensor-img', True, expected_outputs)
    return ret


# Run One touch Match (IDM) using live acquisition - PCSC contactless case
# Acquire fingerdetect and enroll
# Ensure matching sensor img
# Ensure other finger doesnt match
def run_one_touch_match_live_case(config):

    PORT = config.port
    INIT_MODE = config.init_mode

    print_info_message("", False, True, False)
    print_info_message('********** Running ' + run_one_touch_match_live_case.__name__ + ' **********', False, True,
                       False)

    # ENROLL PHASE
    print_info_message("", False, True, False)
    print_info_message("---Enrollment Phase---", False, True, False)
    # Run Power cycle
    auto_or_manual_power_cycle()
    # Run init
    ret = test_tool_cmd(PORT, 'initialize', INIT_MODE)
    if ret != 0:
        return ret
    # Ask user to hold finger on sensor
    print_warning_message("Hold finger on sensor", False, True, False)
    time_out_sec(2)
    # Run acquire finger detect
    ret = test_tool_cmd(PORT, 'acquire', 'fingerdetect')
    if ret != 0:
        return ret
    # Run enroll
    ret = test_tool_cmd(PORT, 'dm_single_enroll', '2 6 1 1')
    if ret != 0:
        return ret
    # MATCH PHASE
    print_info_message("", False, True, False)
    print_info_message("---Match Phase---", False, True, False)
    # Run Power cycle
    auto_or_manual_power_cycle()
    # Run init
    ret = test_tool_cmd(PORT, 'initialize', INIT_MODE)
    if ret != 0:
        return ret
    # Ask user to hold finger on sensor
    print_warning_message("Hold same finger on sensor", False, True, False)
    time_out_sec(2)
    # Run sensor-img match
    ret = test_tool_cmd(PORT, 'dm_match', 'sensor-img')
    if ret != 0:
        return ret
    # Ask user to hold finger on sensor
    print_warning_message("Hold other finger on sensor", False, True, False)
    time_out_sec(2)
    expected_outputs = ['templates do not match']
    ret = test_tool_cmd(PORT, 'dm_match', 'sensor-img', True, expected_outputs)

    return ret


# Run One touch Match (IDM) using images loaded from PC - PCSC contactless case
# Load test image and enroll
# Enusre matching with test image
# Load other test image
# Ensure that doesn't match
def run_one_touch_match_loaded_case(config):

    PORT = config.port
    INIT_MODE = config.init_mode

    print_info_message("", False, True, False)
    print_info_message('********** Running ' + run_one_touch_match_loaded_case.__name__ + ' **********', False, True,
                       False)
    # ENROLL PHASE
    print_info_message("", False, True, False)
    print_info_message("---Enrollment Phase---", False, True, False)
    # Run Power cycle
    auto_or_manual_power_cycle()
    # Run init
    ret = test_tool_cmd(PORT, 'initialize', INIT_MODE)
    if ret != 0:
        return ret
    # Run loadimage test1
    ret = test_tool_cmd(PORT, 'load_image',
                        script_path + "\\" + script_database_dirname + "\\" + script_one_touch_match_loaded_case_dirname + "\\" + script_one_touch_match_loaded_case_images_dirname + "\\" + 'test1.bmp')
    if ret != 0:
        return ret
    # Run enroll
    ret = test_tool_cmd(PORT, 'dm_single_enroll', '2 6 1 1')
    if ret != 0:
        return ret
    # MATCH PHASE
    print_info_message("", False, True, False)
    print_info_message("---Match Phase---", False, True, False)
    # Run Power cycle
    auto_or_manual_power_cycle()
    # Run init
    ret = test_tool_cmd(PORT, 'initialize', INIT_MODE)
    if ret != 0:
        return ret
    # Run loadimage test1
    ret = test_tool_cmd(PORT, 'load_image',
                        script_path + "\\" + script_database_dirname + "\\" + script_one_touch_match_loaded_case_dirname + "\\" + script_one_touch_match_loaded_case_images_dirname + "\\" + 'test1.bmp')
    if ret != 0:
        return ret
    # Run dmmatch loadimage
    ret = test_tool_cmd(PORT, 'dm_match', 'load-img')
    if ret != 0:
        return ret
    # Run loadimage test2
    ret = test_tool_cmd(PORT, 'load_image',
                        script_path + "\\" + script_database_dirname + "\\" + script_one_touch_match_loaded_case_dirname + "\\" + script_one_touch_match_loaded_case_images_dirname + "\\" + 'test2.bmp')
    if ret != 0:
        return ret
    # Run dmmatch loadimage
    expected_outputs = ['templates do not match', 'result - 0x00']
    ret = test_tool_cmd(PORT, 'dm_match', 'load-img', True, expected_outputs)

    return ret


# Run DM_case
# Acquire live 2 6 1 1
# Match sensor img mwf 7902
# Match sensor img mwf 3500
# Match sensor img mwf 1000
# Match sensor img mwf 100
def run_dm_case(config):
    PORT = config.port
    INIT_MODE = config.init_mode

    print_info_message("", False, True, False)
    print_info_message('********** Running ' + run_dm_case.__name__ + ' **********', False, True, False)
    # ENROLL PHASE
    print_info_message("", False, True, False)
    print_info_message("---Enrollment Phase---", False, True, False)
    # Run Power cycle
    auto_or_manual_power_cycle()
    # Run init
    ret = test_tool_cmd(PORT, 'initialize', INIT_MODE)
    if ret != 0:
        return ret
    # Ask user to hold finger on sensor
    print_warning_message("Hold finger on sensor", False, True, False)
    time_out_sec(2)
    # Run acquire live
    ret = test_tool_cmd(PORT, 'acquire', 'live')
    if ret != 0:
        return ret
    # Run enroll
    ret = test_tool_cmd(PORT, 'dm_single_enroll', '2 6 1 1')
    if ret != 0:
        return ret
    # MATCH PHASE
    print_info_message("", False, True, False)
    print_info_message("---Match Phase---", False, True, False)
    # Run Power cycle
    auto_or_manual_power_cycle()
    # Run init
    ret = test_tool_cmd(PORT, 'initialize', INIT_MODE)
    if ret != 0:
        return ret
    # Ask user to hold finger on sensor
    print_warning_message("Hold same finger on sensor", False, True, False)
    time_out_sec(2)
    # Run dmmatch loadimage
    ret = test_tool_cmd(PORT, 'dm_match', 'sensor-img timestamp', 'mwf7902')
    if ret != 0:
        return ret
    # Ask user to hold finger on sensor
    print_warning_message("Hold same finger on sensor", False, True, False)
    time_out_sec(2)
    ret = test_tool_cmd(PORT, 'dm_match', 'sensor-img timestamp', 'mwf3500')
    if ret != 0:
        return ret
    # Ask user to hold finger on sensor
    print_warning_message("Hold same finger on sensor", False, True, False)
    time_out_sec(2)
    ret = test_tool_cmd(PORT, 'dm_match', 'sensor-img timestamp', 'mwf1000')
    if ret != 0:
        return ret
    # Ask user to hold finger on sensor
    print_warning_message("Hold same finger on sensor", False, True, False)
    time_out_sec(2)
    ret = test_tool_cmd(PORT, 'dm_match', 'sensor-img timestamp', 'mwf100')

    return ret