from Framework import *
import time

# params from 0, 10, 11, 12, 14, 18  may be different depending on sensor type Aurora or Polaris
# pre-defined set-up required before starting test

# Run get_params
def get_params(config):

    PORT = config.port
    INIT_MODE = config.init_mode


    print_info_message("", False, True, False)
    print_info_message('********** Running ' + get_params.__name__ + ' **********', False, True, False)
    # Run init
    ret = test_tool_cmd(PORT, 'initialize', INIT_MODE)
    if ret != 0:
        return ret
    # Run get 0
    expected_outputs = 'param[0] = 10'
    ret = test_tool_cmd(PORT, 'get_param', str(0), False, expected_outputs)
    if ret != 0:
        return ret
    # Run get 6
    expected_outputs = 'param[6] = 65535'
    ret = test_tool_cmd(PORT, 'get_param', str(6), False, expected_outputs)
    if ret != 0:
        return ret
    # Run get 7
    expected_outputs = 'param[7] = 15000'
    ret = test_tool_cmd(PORT, 'get_param', str(7), False, expected_outputs)
    if ret != 0:
        return ret
    # Run get 10
    expected_outputs = 'param[10] = 132'
    ret = test_tool_cmd(PORT, 'get_param', str(10), False, expected_outputs)
    if ret != 0:
        return ret
    # Run get 11
    expected_outputs = 'param[11] = 131'
    ret = test_tool_cmd(PORT, 'get_param', str(11), False, expected_outputs)
    if ret != 0:
        return ret
    # Run get 12
    expected_outputs = 'param[12] = 8'
    ret = test_tool_cmd(PORT, 'get_param', str(12), False, expected_outputs)
    if ret != 0:
        return ret
    # Run get 14
    expected_outputs = 'param[14] = 350'
    ret = test_tool_cmd(PORT, 'get_param', str(14), False, expected_outputs)
    if ret != 0:
        return ret
    # Run get 17
    expected_outputs = 'param[17] = 50'
    ret = test_tool_cmd(PORT, 'get_param', str(17), False, expected_outputs)
    if ret != 0:
        return ret
    # Run get 18
    expected_outputs = 'param[18] = 80'
    ret = test_tool_cmd(PORT, 'get_param', str(18), False, expected_outputs)
    if ret != 0:
        return ret
    # Run get 19
    expected_outputs = 'param[19] = 0'
    ret = test_tool_cmd(PORT, 'get_param', str(19), False, expected_outputs)
    if ret != 0:
        return ret
    # Run get 23
    expected_outputs = 'device return code=0x6a80 Bad parameters'
    ret = test_tool_cmd(PORT, 'get_param', str(23), True, expected_outputs)
    if ret != 0:
        return ret
    # Run get 30
    expected_outputs = 'param[30] = 8'
    ret = test_tool_cmd(PORT, 'get_param', str(30), False, expected_outputs)
    if ret != 0:
        return ret
    # Run get 33
    expected_outputs = 'device return code=0x6a80 Bad parameters'
    ret = test_tool_cmd(PORT, 'get_param', str(33), True, expected_outputs)
    if ret != 0:
        return ret
    # Run get 128
    expected_outputs = 'device return code=0x6a81 function not supported'
    ret = test_tool_cmd(PORT, 'get_param', str(128), True, expected_outputs)
    if ret != 0:
        return ret
    # Run get 130
    expected_outputs = 'param[130] = 6'
    ret = test_tool_cmd(PORT, 'get_param', str(130), False, expected_outputs)
    if ret != 0:
        return ret
    # Run get 131
    expected_outputs = 'param[131] = 65535'
    ret = test_tool_cmd(PORT, 'get_param', str(131), False, expected_outputs)


    return ret



# Run set 7
def set_7(config):

    PORT = config.port
    INIT_MODE = config.init_mode


    print_info_message("", False, True, False)
    print_info_message('********** Running ' + set_7.__name__ + ' **********', False, True, False)
    # Run init
    ret = test_tool_cmd(PORT, 'initialize', INIT_MODE)
    if ret != 0:
        return ret
    # Run get 7
    expected_outputs = 'param[7] = 15000'
    ret = test_tool_cmd(PORT, 'get_param', str(7), False, expected_outputs)
    if ret != 0:
        return ret
    # Run set 7 10000
    ret = test_tool_cmd(PORT, 'set_param', str(7) + ' ' + str(10000))
    if ret != 0:
        return ret
    # Run get 7
    expected_outputs = 'param[7] = 10000'
    ret = test_tool_cmd(PORT, 'get_param', str(7), False, expected_outputs)
    if ret != 0:
        return ret
    print_warning_message("Waiting for user timeout to happen in 10 seconds, Please don't touch the sensor", False, True, False)
    start = time.time()
    expected_outputs = 'device return code=0x6748 user timeout'
    ret = test_tool_cmd(PORT, 'acquire', 'fingerdetect', True, expected_outputs)
    # if ret != 0:
    #     return ret
    end = time.time()
    if (end - start) > 10.5:
        print_error_message("User timeout exceeded 10 seconds", False)
        ret = 1
    elif (end - start) < 9.5:
        print_error_message("User timeout didn't reach 10 seconds, took only " + str(end - start), False)
    if ret != 0:
        return ret
    # Run Power cycle
    auto_or_manual_power_cycle()
    # Run init
    ret = test_tool_cmd(PORT, 'initialize', INIT_MODE)
    if ret != 0:
        return ret
    # Run get 7
    expected_outputs = 'param[7] = 15000'
    ret = test_tool_cmd(PORT, 'get_param', str(7), False, expected_outputs)
    if ret != 0:
        return ret
    print_warning_message("Waiting for user timeout to happen in 15 seconds. Please don't touch the sensor")
    start = time.time()
    expected_outputs = 'device return code=0x6748 user timeout'
    ret = test_tool_cmd(PORT, 'acquire', 'fingerdetect', True, expected_outputs)
    # if ret != 0:
    #     return ret
    end = time.time()
    if (end - start) > 15.5:
        print_error_message("User timeout exceeded 15 seconds", False)
        ret = 1
    elif (end - start) < 14.5:
        print_error_message("User timeout didn't reach 15 seconds, took only " + str(end - start), False)
    return ret



# Run set 17
def set_17(config):

    PORT = config.port
    INIT_MODE = config.init_mode


    print_info_message("", False, True, False)
    print_info_message('********** Running ' + set_17.__name__ + ' **********', False, True, False)
    # Run init
    ret = test_tool_cmd(PORT, 'initialize', INIT_MODE)
    if ret != 0:
        return ret
    # Run get 17
    expected_outputs = 'param[17] = 50'
    ret = test_tool_cmd(PORT, 'get_param', str(17), False, expected_outputs)
    if ret != 0:
        return ret
    # Run set 17 25
    ret = test_tool_cmd(PORT, 'set_param', str(17) + ' ' + str(25))
    if ret != 0:
        return ret
    # Run get 17
    expected_outputs = 'param[17] = 25'
    ret = test_tool_cmd(PORT, 'get_param', str(17), False, expected_outputs)
    if ret != 0:
        return ret
    # Run Power cycle
    auto_or_manual_power_cycle()
    # Run init
    ret = test_tool_cmd(PORT, 'initialize', INIT_MODE)
    if ret != 0:
        return ret
    # Run get 17
    expected_outputs = 'param[17] = 50'
    ret = test_tool_cmd(PORT, 'get_param', str(17), False, expected_outputs)


    return ret


# Run set 18
def set_18(config):

    PORT = config.port
    INIT_MODE = config.init_mode

    print_info_message("", False, True, False)
    print_info_message('********** Running ' + set_18.__name__ + ' **********', False, True, False)
    # Run init
    ret = test_tool_cmd(PORT, 'initialize', INIT_MODE)
    if ret != 0:
        return ret
    # Run get 18
    expected_outputs = 'param[18] = 100'
    ret = test_tool_cmd(PORT, 'get_param', str(18), False, expected_outputs)
    if ret != 0:
        return ret
    # Run set 18 200
    ret = test_tool_cmd(PORT, 'set_param', str(18) + ' ' + str(200))
    if ret != 0:
        return ret
    # Run get 18
    expected_outputs = 'param[18] = 200'
    ret = test_tool_cmd(PORT, 'get_param', str(18), False, expected_outputs)
    if ret != 0:
        return ret
    # Run Power cycle
    auto_or_manual_power_cycle()
    # Run init
    ret = test_tool_cmd(PORT, 'initialize', INIT_MODE)
    if ret != 0:
        return ret
    # Run get 18
    expected_outputs = 'param[18] = 100'
    ret = test_tool_cmd(PORT, 'get_param', str(18), False, expected_outputs)

    return ret

# Run set 131
def set_131(config):
    PORT = config.port
    INIT_MODE = config.init_mode

    print_info_message("", False, True, False)
    print_info_message('********** Running ' + set_131.__name__ + ' **********', False, True, False)
    # Run init
    ret = test_tool_cmd(PORT, 'initialize', INIT_MODE)
    if ret != 0:
        return ret
    # Run get 131
    expected_outputs = 'param[131] = 65535'
    ret = test_tool_cmd(PORT, 'get_param', str(131), False, expected_outputs)
    if ret != 0:
        return ret
    # Run set 131 10
    ret = test_tool_cmd(PORT, 'set_param', str(131) + ' ' + str(10))
    if ret != 0:
        return ret
    # Run get 131
    expected_outputs = 'param[131] = 10'
    ret = test_tool_cmd(PORT, 'get_param', str(131), False, expected_outputs)
    if ret != 0:
        return ret
    # Run load_image instead of acquire fingerdetect
    ret = test_tool_cmd(PORT, 'load_image', script_path + "\\" + script_database_dirname + "\\" + script_set_param_131_dirname + "\\" + str('1.bmp') + ' ' + str('flip'))
    if ret != 0:
        return ret
    # Run enroll
    # ret = test_tool_cmd(PORT, 'dm_single_enroll', '2 6 1 1')
    # if ret != 0:
    #     return ret
    # Run get_template
    ret = test_tool_cmd(PORT, 'get_template', '')
    if ret != 0:
        return ret
    # Run match_templates
    ret = test_tool_cmd(PORT, 'match_templates', '')
    if ret != 0:
        return ret
    # Run get 132
    expected_outputs = 'param[132] = 27'
    ret = test_tool_cmd(PORT, 'get_param', str(132), False, expected_outputs)
    if ret != 0:
        return ret
    # Run Power cycle
    auto_or_manual_power_cycle()
    # Run init
    ret = test_tool_cmd(PORT, 'initialize', INIT_MODE)
    if ret != 0:
        return ret
    # Run get 131
    expected_outputs = 'param[131] = 65535'
    ret = test_tool_cmd(PORT, 'get_param', str(131), False, expected_outputs)
    if ret != 0:
        return ret
    # Run load_image instead of acquire fingerdetect
    ret = test_tool_cmd(PORT, 'load_image', script_path + "\\" + script_database_dirname + "\\" + script_set_param_131_dirname + "\\" + str('1.bmp') + ' ' + str('flip'))
    if ret != 0:
        return ret
    # Run enroll
    # ret = test_tool_cmd(PORT, 'dm_single_enroll', '2 6 1 1')
    # if ret != 0:
    #     return ret
    # Run get_template
    ret = test_tool_cmd(PORT, 'get_template', '')
    if ret != 0:
        return ret
    # Run match_templates
    ret = test_tool_cmd(PORT, 'match_templates', '')
    if ret != 0:
        return ret
    # Run get 132
    expected_outputs = 'param[132] = 33'
    ret = test_tool_cmd(PORT, 'get_param', str(132), False, expected_outputs)
    if ret != 0:
        return ret
    # Run set 131 -1
    ret = test_tool_cmd(PORT, 'set_param', str(131) + ' ' + str(-1))
    if ret != 0:
        return ret
    # Run get 131
    expected_outputs = 'param[131] = 65535'
    ret = test_tool_cmd(PORT, 'get_param', str(131), False, expected_outputs)
    if ret != 0:
        return ret
    # Run load_image instead of acquire fingerdetect
    ret = test_tool_cmd(PORT, 'load_image', script_path + "\\" + script_database_dirname + "\\" + script_set_param_131_dirname + "\\" + str('1.bmp') + ' ' + str('flip'))
    if ret != 0:
        return ret
    # Run enroll
    # ret = test_tool_cmd(PORT, 'dm_single_enroll', '2 6 1 1')
    # if ret != 0:
    #     return ret
    # Run get_template
    ret = test_tool_cmd(PORT, 'get_template', '')
    if ret != 0:
        return ret
    # Run match_templates
    ret = test_tool_cmd(PORT, 'match_templates', '')
    if ret != 0:
        return ret
    # Run get 132
    expected_outputs = 'param[132] = 33'
    ret = test_tool_cmd(PORT, 'get_param', str(132), False, expected_outputs)
    if ret != 0:
        return ret
    # Run set 131 65535
    ret = test_tool_cmd(PORT, 'set_param', str(131) + ' ' + str(65535))
    if ret != 0:
        return ret
    # Run get 131
    expected_outputs = 'param[131] = 65535'
    ret = test_tool_cmd(PORT, 'get_param', str(131), False, expected_outputs)
    if ret != 0:
        return ret
    # Run load_image instead of acquire fingerdetect
    ret = test_tool_cmd(PORT, 'load_image', script_path + "\\" + script_database_dirname + "\\" + script_set_param_131_dirname + "\\" + str('1.bmp') + ' ' + str('flip'))
    if ret != 0:
        return ret
    # Run enroll
    # ret = test_tool_cmd(PORT, 'dm_single_enroll', '2 6 1 1')
    # if ret != 0:
    #     return ret
    # Run get_template
    ret = test_tool_cmd(PORT, 'get_template', '')
    if ret != 0:
        return ret
    # Run match_templates
    ret = test_tool_cmd(PORT, 'match_templates', '')
    if ret != 0:
        return ret
    # Run get 132
    expected_outputs = 'param[132] = 33'
    ret = test_tool_cmd(PORT, 'get_param', str(132), False, expected_outputs)

    return ret

# Run store_params_131
def store_params_131(config):
    PORT = config.port
    INIT_MODE = config.init_mode

    print_info_message("", False, True, False)
    print_info_message('********** Running ' + store_params_131.__name__ + ' **********', False, True, False)
    # Run init
    ret = test_tool_cmd(PORT, 'initialize', INIT_MODE)
    if ret != 0:
        return ret
    # Run get 131
    expected_outputs = 'param[131] = 65535'
    ret = test_tool_cmd(PORT, 'get_param', str(131), False, expected_outputs)
    if ret != 0:
        return ret
    # Run set 131 10
    ret = test_tool_cmd(PORT, 'set_param', str(131) + ' ' + str(10))
    if ret != 0:
        return ret
    # Run get 131
    expected_outputs = 'param[131] = 10'
    ret = test_tool_cmd(PORT, 'get_param', str(131), False, expected_outputs)
    if ret != 0:
        return ret
    # Run store_params
    ret = test_tool_cmd(PORT, 'store_params', '')
    if ret != 0:
        return ret
    # Run load_image instead of acquire fingerdetect
    ret = test_tool_cmd(PORT, 'load_image', script_path + "\\" + script_database_dirname + "\\" + script_set_param_131_dirname + "\\" + str('1.bmp') + ' ' + str('flip'))
    if ret != 0:
        return ret
    # Run get_template
    ret = test_tool_cmd(PORT, 'get_template', '')
    if ret != 0:
        return ret
    # Run match_templates
    ret = test_tool_cmd(PORT, 'match_templates', '')
    if ret != 0:
        return ret
    # Run get 132
    expected_outputs = 'param[132] = 27'
    ret = test_tool_cmd(PORT, 'get_param', str(132), False, expected_outputs)
    if ret != 0:
        return ret
    # Run Power cycle
    auto_or_manual_power_cycle()
    # Run init
    ret = test_tool_cmd(PORT, 'initialize', INIT_MODE)
    if ret != 0:
        return ret
    # Run get 131
    expected_outputs = 'param[131] = 10'
    ret = test_tool_cmd(PORT, 'get_param', str(131), False, expected_outputs)
    if ret != 0:
        return ret
    # Run load_image instead of acquire fingerdetect
    ret = test_tool_cmd(PORT, 'load_image', script_path + "\\" + script_database_dirname + "\\" + script_set_param_131_dirname + "\\" + str('1.bmp') + ' ' + str('flip'))
    if ret != 0:
        return ret
    # Run get_template
    ret = test_tool_cmd(PORT, 'get_template', '')
    if ret != 0:
        return ret
    # Run match_templates
    ret = test_tool_cmd(PORT, 'match_templates', '')
    if ret != 0:
        return ret
    # Run get 132
    expected_outputs = 'param[132] = 27'
    ret = test_tool_cmd(PORT, 'get_param', str(132), False, expected_outputs)
    if ret != 0:
        return ret
    # Run set 131 -1
    ret = test_tool_cmd(PORT, 'set_param', str(131) + ' ' + str(-1))
    if ret != 0:
        return ret
    # Run get 131
    expected_outputs = 'param[131] = 65535'
    ret = test_tool_cmd(PORT, 'get_param', str(131), False, expected_outputs)
    if ret != 0:
        return ret
    # Run load_image instead of acquire fingerdetect
    ret = test_tool_cmd(PORT, 'load_image', script_path + "\\" + script_database_dirname + "\\" + script_set_param_131_dirname + "\\" + str('1.bmp') + ' ' + str('flip'))
    if ret != 0:
        return ret
    ret = test_tool_cmd(PORT, 'get_template', '')
    if ret != 0:
        return ret
    # Run match_templates
    ret = test_tool_cmd(PORT, 'match_templates', '')
    if ret != 0:
        return ret
    # Run get 132
    expected_outputs = 'param[132] = 33'
    ret = test_tool_cmd(PORT, 'get_param', str(132), False, expected_outputs)
    if ret != 0:
        return ret
    # Run Power cycle
    auto_or_manual_power_cycle()
    # Run init
    ret = test_tool_cmd(PORT, 'initialize', INIT_MODE)
    if ret != 0:
        return ret
    # Run get 131
    expected_outputs = 'param[131] = 10'
    ret = test_tool_cmd(PORT, 'get_param', str(131), False, expected_outputs)
    if ret != 0:
        return ret
    # Run set 131 10
    ret = test_tool_cmd(PORT, 'set_param', str(131) + ' ' + str(-1))
    if ret != 0:
        return ret
    # Run store_params
    ret = test_tool_cmd(PORT, 'store_params', '')
    if ret != 0:
        return ret
    expected_outputs = 'param[131] = 65535'
    ret = test_tool_cmd(PORT, 'get_param', str(131), False, expected_outputs)

    return ret


# Run store_params_7
def store_params_7(config):
    PORT = config.port
    INIT_MODE = config.init_mode

    print_info_message("", False, True, False)
    print_info_message('********** Running ' + store_params_7.__name__ + ' **********', False, True, False)
    # Run init
    ret = test_tool_cmd(PORT, 'initialize', INIT_MODE)
    if ret != 0:
        return ret
    # Run get 7
    expected_outputs = 'param[7] = 15000'
    ret = test_tool_cmd(PORT, 'get_param', str(7), False, expected_outputs)
    if ret != 0:
        return ret
    # Run set 7 14000
    ret = test_tool_cmd(PORT, 'set_param', str(7) + ' ' + str(14000))
    if ret != 0:
        return ret
    # Run get 7
    expected_outputs = 'param[7] = 14000'
    ret = test_tool_cmd(PORT, 'get_param', str(7), False, expected_outputs)
    if ret != 0:
        return ret
    # Run store_params
    ret = test_tool_cmd(PORT, 'store_params', '')
    if ret != 0:
        return ret
    # Run get 7
    expected_outputs = 'param[7] = 14000'
    ret = test_tool_cmd(PORT, 'get_param', str(7), False, expected_outputs)
    if ret != 0:
        return ret
    # Run Power cycle
    auto_or_manual_power_cycle()
    # Run init
    ret = test_tool_cmd(PORT, 'initialize', INIT_MODE)
    if ret != 0:
        return ret
    # Run get 7
    expected_outputs = 'param[7] = 14000'
    ret = test_tool_cmd(PORT, 'get_param', str(7), False, expected_outputs)
    if ret != 0:
        return ret
    # Run set 7 15000
    ret = test_tool_cmd(PORT, 'set_param', str(7) + ' ' + str(15000))
    if ret != 0:
        return ret
    # Run store_params
    ret = test_tool_cmd(PORT, 'store_params', '')

    return ret

# Run restore_params_7
def restore_params_7(config):
    PORT = config.port
    INIT_MODE = config.init_mode

    print_info_message("", False, True, False)
    print_info_message('********** Running ' + restore_params_7.__name__ + ' **********', False, True, False)
    # Run init
    ret = test_tool_cmd(PORT, 'initialize', INIT_MODE)
    if ret != 0:
        return ret
    # Run get 7
    expected_outputs = 'param[7] = 15000'
    ret = test_tool_cmd(PORT, 'get_param', str(7), False, expected_outputs)
    if ret != 0:
        return ret
    # Run set 7 13000
    ret = test_tool_cmd(PORT, 'set_param', str(7) + ' ' + str(13000))
    if ret != 0:
        return ret
    # Run get 7
    expected_outputs = 'param[7] = 13000'
    ret = test_tool_cmd(PORT, 'get_param', str(7), False, expected_outputs)
    if ret != 0:
        return ret
    # Run restore_params
    ret = test_tool_cmd(PORT, 'restore_params', '')
    if ret != 0:
        return ret
    # Run get 7
    expected_outputs = 'param[7] = 15000'
    ret = test_tool_cmd(PORT, 'get_param', str(7), False, expected_outputs)
    if ret != 0:
        return ret
    # Run Power cycle
    auto_or_manual_power_cycle()
    # Run init
    ret = test_tool_cmd(PORT, 'initialize', INIT_MODE)
    if ret != 0:
        return ret
    # Run get 7
    expected_outputs = 'param[7] = 15000'
    ret = test_tool_cmd(PORT, 'get_param', str(7), False, expected_outputs)

    return ret


# Run store_params_17
def store_params_17(config):
    PORT = config.port
    INIT_MODE = config.init_mode

    print_info_message("", False, True, False)
    print_info_message('********** Running ' + store_params_17.__name__ + ' **********', False, True, False)
    # Run init
    ret = test_tool_cmd(PORT, 'initialize', INIT_MODE)
    if ret != 0:
        return ret
    # Run get 17
    expected_outputs = 'param[17] = 50'
    ret = test_tool_cmd(PORT, 'get_param', str(17), False, expected_outputs)
    if ret != 0:
        return ret
    # Run set 17 25
    ret = test_tool_cmd(PORT, 'set_param', str(17) + ' ' + str(25))
    if ret != 0:
        return ret
    # Run get 17
    expected_outputs = 'param[17] = 25'
    ret = test_tool_cmd(PORT, 'get_param', str(17), False, expected_outputs)
    if ret != 0:
        return ret
    # Run store_params
    ret = test_tool_cmd(PORT, 'store_params', '')
    if ret != 0:
        return ret
    # Run get 17
    expected_outputs = 'param[17] = 25'
    ret = test_tool_cmd(PORT, 'get_param', str(17), False, expected_outputs)
    if ret != 0:
        return ret
    # Run Power cycle
    auto_or_manual_power_cycle()
    # Run init
    ret = test_tool_cmd(PORT, 'initialize', INIT_MODE)
    if ret != 0:
        return ret
    # Run get 17
    expected_outputs = 'param[17] = 25'
    ret = test_tool_cmd(PORT, 'get_param', str(17), False, expected_outputs)
    if ret != 0:
        return ret
    # Run set 17 50
    ret = test_tool_cmd(PORT, 'set_param', str(17) + ' ' + str(50))
    if ret != 0:
        return ret
    # Run store_params
    ret = test_tool_cmd(PORT, 'store_params', '')

    return ret

# Run restore_params_17
def restore_params_17(config):
    PORT = config.port
    INIT_MODE = config.init_mode

    print_info_message("", False, True, False)
    print_info_message('********** Running ' + restore_params_17.__name__ + ' **********', False, True, False)
    # Run init
    ret = test_tool_cmd(PORT, 'initialize', INIT_MODE)
    if ret != 0:
        return ret
    # Run get 17
    expected_outputs = 'param[17] = 50'
    ret = test_tool_cmd(PORT, 'get_param', str(17), False, expected_outputs)
    if ret != 0:
        return ret
    # Run set 17 40
    ret = test_tool_cmd(PORT, 'set_param', str(17) + ' ' + str(40))
    if ret != 0:
        return ret
    # Run get 17
    expected_outputs = 'param[17] = 40'
    ret = test_tool_cmd(PORT, 'get_param', str(17), False, expected_outputs)
    if ret != 0:
        return ret
    # Run restore_params
    ret = test_tool_cmd(PORT, 'restore_params', '')
    if ret != 0:
        return ret
    # Run get 17
    expected_outputs = 'param[17] = 50'
    ret = test_tool_cmd(PORT, 'get_param', str(17), False, expected_outputs)
    if ret != 0:
        return ret
    # Run Power cycle
    auto_or_manual_power_cycle()
    # Run init
    ret = test_tool_cmd(PORT, 'initialize', INIT_MODE)
    if ret != 0:
        return ret
    # Run get 17
    expected_outputs = 'param[17] = 50'
    ret = test_tool_cmd(PORT, 'get_param', str(17), False, expected_outputs)

    return ret