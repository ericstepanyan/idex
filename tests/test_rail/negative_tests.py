from Framework import *


# run get_param_99
def get_param_99(config):

    PORT = config.port
    INIT_MODE = config.init_mode


    print_info_message("", False, True, False)
    print_info_message('********** Running ' + get_param_99.__name__ + ' **********', False, True, False)
    # Run init
    ret = test_tool_cmd(PORT, 'initialize', INIT_MODE)
    if ret != 0:
        return ret
    # Run get 99
    expected_outputs = 'device return code=0x6a80 Bad parameters'
    ret = test_tool_cmd(PORT, 'get_param', str(99), True, expected_outputs)

    return ret


# Run set_param_99_0
def set_param_99_0(config):

    PORT = config.port
    INIT_MODE = config.init_mode


    print_info_message("", False, True, False)
    print_info_message('********** Running ' + set_param_99_0.__name__ + ' **********', False, True, False)
    # Run init
    ret = test_tool_cmd(PORT, 'initialize', INIT_MODE)
    if ret != 0:
        return ret
    # Run set 99 0
    expected_outputs = 'device return code=0x6a80 Bad parameters'
    ret = test_tool_cmd(PORT, 'set_param', str(99) + ' ' + str(0), True, expected_outputs)

    return ret


# Run set_param_10_35
def set_param_10_35(config):

    PORT = config.port
    INIT_MODE = config.init_mode


    print_info_message("", False, True, False)
    print_info_message('********** Running ' + set_param_10_35.__name__ + ' **********', False, True, False)
    # Run init
    ret = test_tool_cmd(PORT, 'initialize', INIT_MODE)
    if ret != 0:
        return ret
    # Run set_param 10 35
    expected_outputs = 'device return code=0x6a80 Bad parameters'
    ret = test_tool_cmd(PORT, 'set_param', str(10) + ' ' + str(35), True, expected_outputs)

    return ret


# Run set_param_30
def set_param_30(config):

    PORT = config.port
    INIT_MODE = config.init_mode


    print_info_message("", False, True, False)
    print_info_message('********** Running ' + set_param_30.__name__ + ' **********', False, True, False)
    # Run init
    ret = test_tool_cmd(PORT, 'initialize', INIT_MODE)
    if ret != 0:
        return ret
    # Run get 30
    expected_outputs = 'param[30] = 8'
    ret = test_tool_cmd(PORT, 'get_param', str(30), False, expected_outputs)
    if ret != 0:
        return ret
    # Run set_param 30 3
    expected_outputs = 'device return code=0x6a80 Bad parameters'
    ret = test_tool_cmd(PORT, 'set_param', str(30) + ' ' + str(3), True, expected_outputs)
    if ret != 0:
        return ret
    # Run get 30
    expected_outputs = 'param[30] = 8'
    ret = test_tool_cmd(PORT, 'get_param', str(30), False, expected_outputs)
    if ret != 0:
        return ret
    # Run set_param 30 11
    expected_outputs = 'device return code=0x6a80 Bad parameters'
    ret = test_tool_cmd(PORT, 'set_param', str(30) + ' ' + str(11), True, expected_outputs)
    if ret != 0:
        return ret
    # Run get 30
    expected_outputs = 'param[30] = 8'
    ret = test_tool_cmd(PORT, 'get_param', str(30), False, expected_outputs)
    if ret != 0:
        return ret
    # Run Power cycle
    auto_or_manual_power_cycle()
    # Run init
    ret = test_tool_cmd(PORT, 'initialize', INIT_MODE)
    if ret != 0:
        return ret
    # Run get 30
    expected_outputs = 'param[30] = 8'
    ret = test_tool_cmd(PORT, 'get_param', str(30), False, expected_outputs)

    return ret

# Run invalid_sequence_get_template_without_acquire
def invalid_sequence_get_template_without_acquire(config):

    PORT = config.port
    INIT_MODE = config.init_mode


    print_info_message("", False, True, False)
    print_info_message('********** Running ' + invalid_sequence_get_template_without_acquire.__name__ + ' **********', False, True, False)
    # Run Power cycle
    auto_or_manual_power_cycle()
    # Run init
    ret = test_tool_cmd(PORT, 'initialize', INIT_MODE)
    if ret != 0:
        return ret
    # Run get_template
    expected_outputs = 'device return code=0x6985 wrong state'
    ret = test_tool_cmd(PORT, 'get_template', '', True, expected_outputs)
    if ret != 0:
        return ret
    # Run Power cycle
    auto_or_manual_power_cycle()
    # Run init
    ret = test_tool_cmd(PORT, 'initialize', INIT_MODE)
    if ret != 0:
        return ret
    # Run get_enroll_template
    expected_outputs = 'device return code=0x6985 wrong state'
    ret = test_tool_cmd(PORT, 'get_enroll_template', '', True, expected_outputs)

    return ret


# Run invalid_sequence_get_template_without_acquire
def invalid_sequence_acquire_without_initialize(config):

    PORT = config.port
    INIT_MODE = config.init_mode


    print_info_message("", False, True, False)
    print_info_message('********** Running ' + invalid_sequence_acquire_without_initialize.__name__ + ' **********', False, True, False)
    # Run uninit
    ret = test_tool_cmd(PORT, 'uninitialize', '')
    if ret != 0:
        return ret
    # Run acquire live
    expected_outputs = 'device return code=0x6985 wrong state'
    ret = test_tool_cmd(PORT, 'acquire', 'live', True, expected_outputs)

    return ret

# run get_param_25
def get_param_25(config):

    PORT = config.port
    INIT_MODE = config.init_mode


    print_info_message("", False, True, False)
    print_info_message('********** Running ' + get_param_25.__name__ + ' **********', False, True, False)
    # Run init
    ret = test_tool_cmd(PORT, 'initialize', INIT_MODE)
    if ret != 0:
        return ret
    # Run get 25
    expected_outputs = 'device return code=0x6a80 Bad parameters'
    ret = test_tool_cmd(PORT, 'get_param', str(25), True, expected_outputs)

    return ret

# run unsupported_command_template
def unsupported_command_template(config):

    PORT = config.port
    INIT_MODE = config.init_mode


    print_info_message("", False, True, False)
    print_info_message('********** Running ' + unsupported_command_template.__name__ + ' **********', False, True, False)
    # Run init
    ret = test_tool_cmd(PORT, 'initialize', INIT_MODE)
    if ret != 0:
        return ret
    # Run template
    expected_outputs = 'command not found!'
    ret = test_tool_cmd(PORT, 'template 0', 'template.bin', True, expected_outputs)

    return ret

# run unsupported_command_details
def unsupported_command_details(config):

    PORT = config.port
    INIT_MODE = config.init_mode


    print_info_message("", False, True, False)
    print_info_message('********** Running ' + unsupported_command_details.__name__ + ' **********', False, True, False)
    # Run init
    ret = test_tool_cmd(PORT, 'initialize', INIT_MODE)
    if ret != 0:
        return ret
    # Run template
    expected_outputs = 'device return code=0x6d00 command is not valid or not implemented'
    ret = test_tool_cmd(PORT, 'details 0', '', True, expected_outputs)

    return ret

# run unsupported_command_key
def unsupported_command_key(config):

    PORT = config.port
    INIT_MODE = config.init_mode


    print_info_message("", False, True, False)
    print_info_message('********** Running ' + unsupported_command_key.__name__ + ' **********', False, True, False)
    # Run init
    ret = test_tool_cmd(PORT, 'initialize', INIT_MODE)
    if ret != 0:
        return ret
    # Run template
    expected_outputs = 'command not found!'
    ret = test_tool_cmd(PORT, 'key create root', 'key.bin', True, expected_outputs)

    return ret

# run unsupported_command_format
def unsupported_command_format(config):

    PORT = config.port
    INIT_MODE = config.init_mode


    print_info_message("", False, True, False)
    print_info_message('********** Running ' + unsupported_command_format.__name__ + ' **********', False, True, False)
    # Run init
    ret = test_tool_cmd(PORT, 'initialize', INIT_MODE)
    if ret != 0:
        return ret
    # Run template
    expected_outputs = 'command not found!'
    ret = test_tool_cmd(PORT, 'format', '18348', True, expected_outputs)

    return ret

# run unsupported_command_fat
def unsupported_command_fat(config):

    PORT = config.port
    INIT_MODE = config.init_mode


    print_info_message("", False, True, False)
    print_info_message('********** Running ' + unsupported_command_fat.__name__ + ' **********', False, True, False)
    # Run init
    ret = test_tool_cmd(PORT, 'initialize', INIT_MODE)
    if ret != 0:
        return ret
    # Run template
    expected_outputs = 'device return code=0x6d00 command is not valid or not implemented'
    ret = test_tool_cmd(PORT, 'fat', '', True, expected_outputs)

    return ret

# run unsupported_command_flash
def unsupported_command_flash(config):

    PORT = config.port
    INIT_MODE = config.init_mode


    print_info_message("", False, True, False)
    print_info_message('********** Running ' + unsupported_command_flash.__name__ + ' **********', False, True, False)
    # Run init
    ret = test_tool_cmd(PORT, 'initialize', INIT_MODE)
    if ret != 0:
        return ret
    # Run template
    expected_outputs = 'command not found!'
    ret = test_tool_cmd(PORT, 'flash', '', True, expected_outputs)

    return ret

