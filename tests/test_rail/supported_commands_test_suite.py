from Framework import *


########## This suite cases require pre defined setup which will tell the versions(hw,fw,matcher, mcu) and existence or absence of sensor calibration data




# Run test_initialize
def test_initialize(config):

    PORT = config.port
    INIT_MODE = config.init_mode


    print_info_message("", False, True, False)
    print_info_message('********** Running ' + test_initialize.__name__ + ' **********', False, True, False)
    # Run Power cycle
    auto_or_manual_power_cycle()
    # Run init
    ret = test_tool_cmd(PORT, 'initialize', INIT_MODE)

    return ret

# Run test_initialize_repetitive
def test_initialize_repetitive(config):

    PORT = config.port
    INIT_MODE = config.init_mode


    print_info_message("", False, True, False)
    print_info_message('********** Running ' + test_initialize_repetitive.__name__ + ' **********', False, True, False)
    print_warning_message("Ensure that sensor is properly calibrated before starting the case", False)
    # Run init
    ret = test_tool_cmd(PORT, 'initialize', INIT_MODE)
    if ret != 0:
        return ret
    # Run init
    ret = test_tool_cmd(PORT, 'initialize', INIT_MODE)
    if ret != 0:
        return ret
    # Run init
    ret = test_tool_cmd(PORT, 'initialize', INIT_MODE)
    if ret != 0:
        return ret
    # Run acquire fingerdetect
    print_warning_message("Place finger on the sensor", False, True, False)
    ret = test_tool_cmd(PORT, 'acquire', 'fingerdetect')
    if ret != 0:
        return ret
    # Run get image
    ret = test_tool_cmd(PORT, 'get_image', 'image_after_mult_init_finger_01.bmp')
    if ret != 0:
        return ret
    # Run check that image was created
    if os.path.exists(idex_test_directory_path + '\\' + 'image_after_mult_init_finger_01.bmp'):
        print_successful_message("Image was successfully created", False, True, False)
        print_warning_message("Check that image has good quality", False)
        # ret = 0
        # return ret
    else:
        print_error_message("Image was not created", False, True, True)
        ret = 1
        return ret
    # Run acquire live
    ret = test_tool_cmd(PORT, 'acquire', 'live')
    if ret != 0:
        return ret
    # Run get image
    ret = test_tool_cmd(PORT, 'get_image', 'image_after_mult_init_live_01.bmp')
    if ret != 0:
        return ret
    if os.path.exists(idex_test_directory_path + '\\' + 'image_after_mult_init_live_01.bmp'):
        print_successful_message("Image was successfully created", False, True, False)
        print_warning_message("Check that image is clean", False)
        # ret = 0
        # return ret
    else:
        print_error_message("Image was not created", False, True, True)
        ret = 1
        return ret
    # Run uninit
    ret = test_tool_cmd(PORT, 'uninitialize', '')

    return ret


# Run test_uninitialize
def test_uninitialize(config):

    PORT = config.port
    INIT_MODE = config.init_mode


    print_info_message("", False, True, False)
    print_info_message('********** Running ' + test_uninitialize.__name__ + ' **********', False, True, False)

    # Run init
    ret = test_tool_cmd(PORT, 'initialize', INIT_MODE)
    if ret != 0:
        return ret
    # Run uninit
    ret = test_tool_cmd(PORT, 'uninitialize', '')
    if ret != 0:
        return ret
    # Run uninit
    expected_outputs = 'device return code=0x6985 wrong state'
    ret = test_tool_cmd(PORT, 'uninitialize', '', True, expected_outputs)
    if ret != 0:
        return ret
    # Run uninit
    expected_outputs = 'device return code=0x6985 wrong state'
    ret = test_tool_cmd(PORT, 'uninitialize', '', True, expected_outputs)
    if ret != 0:
        return ret
    # Run init
    ret = test_tool_cmd(PORT, 'initialize', INIT_MODE)
    if ret != 0:
        return ret
    # Run calibrate
    ret = test_tool_cmd(PORT, 'calibrate', 'performCal force')
    if ret != 0:
        return ret
    # Run acquire fingerdetect
    print_warning_message("Place finger on the sensor", False, True, False)
    ret = test_tool_cmd(PORT, 'acquire', 'fingerdetect')
    if ret != 0:
        return ret
    # Run get image
    ret = test_tool_cmd(PORT, 'get_image', 'image_after_uninit_01.bmp')
    if ret != 0:
        return ret
    # Run check that image was created
    if os.path.exists(idex_test_directory_path + '\\' + 'image_after_uninit_01.bmp'):
        print_successful_message("Image was successfully created", False, True, False)
        print_warning_message("Check that image has good quality", False)
        ret = 0
        return ret
    else:
        print_error_message("Image was not created", False, True, True)
        ret = 1
        return ret


# Run get_sensor_fw_version
def get_sensor_fw_version(config):

    PORT = config.port
    INIT_MODE = config.init_mode


    print_info_message("", False, True, False)
    print_info_message('********** Running ' + get_sensor_fw_version.__name__ + ' **********', False, True, False)

    # Run init
    ret = test_tool_cmd(PORT, 'initialize', INIT_MODE)
    if ret != 0:
        return ret
    # Run get_fw_version
    expected_outputs = [data['FW version']]
    ret = test_tool_cmd(PORT, 'get_fw_version', '', False, expected_outputs)

    return ret

# Run get_version
def get_version(config):

    PORT = config.port
    INIT_MODE = config.init_mode


    print_info_message("", False, True, False)
    print_info_message('********** Running ' + get_version.__name__ + ' **********', False, True, False)

    # Run init
    ret = test_tool_cmd(PORT, 'initialize', INIT_MODE)
    if ret != 0:
        return ret
    # Run get_version
    expected_outputs = [data['SE version'], data['MCU version'], data['Matcher version'], data['FW version'],data['Sensor info']]
    ret = test_tool_cmd(PORT, 'get_version', '', False, expected_outputs)

    return ret

# Run get_uid
def get_uid(config):

    PORT = config.port
    INIT_MODE = config.init_mode


    print_info_message("", False, True, False)
    print_info_message('********** Running ' + get_uid.__name__ + ' **********', False, True, False)

    # Run init
    ret = test_tool_cmd(PORT, 'initialize', INIT_MODE)
    if ret != 0:
        return ret
    # Run get_version
    expected_outputs = ['mcu uid', 'sensor uid', 'hw uid']
    ret = test_tool_cmd(PORT, 'get_uid', '', False, expected_outputs)

    return ret

# Run test_calibrate
def test_calibrate(config):

    PORT = config.port
    INIT_MODE = config.init_mode


    print_info_message("", False, True, False)
    print_info_message('********** Running ' + test_calibrate.__name__ + ' **********', False, True, False)
    print_warning_message("This test will fail if sensor OTP has calibration data stored previously", False)
    # Run init
    ret = test_tool_cmd(PORT, 'initialize', INIT_MODE)
    if ret != 0:
        return ret
    # Run calibrate_performCal
    ret = test_tool_cmd(PORT, 'calibrate', 'performCal')

    return ret

# Run test_calibrate_force
def test_calibrate_force(config):

    PORT = config.port
    INIT_MODE = config.init_mode


    print_info_message("", False, True, False)
    print_info_message('********** Running ' + test_calibrate_force.__name__ + ' **********', False, True, False)

    # Run init
    ret = test_tool_cmd(PORT, 'initialize', INIT_MODE)
    if ret != 0:
        return ret
    # Run calibrate_performCal
    expected_outputs = 'device return code=0x6743 wrong calibration data (EAGAIN)'
    ret = test_tool_cmd(PORT, 'calibrate', 'performCal', True, expected_outputs)
    if ret != 0:
        return ret
    # Run calibrate_performCal_force
    ret = test_tool_cmd(PORT, 'calibrate', 'performCal force')

    return ret


################################ 'sensor command test' must also be written in this suite ################################