from Framework import *


# PARAMETERS TABLE

value = ('device return code=0x6a80 Bad parameters', 1)
d = dict()

for x in range(0, 256):
    d[x] = value

d[0] = (10, 1)
d[6] = (65535, 0)
d[7] = (15000, 0)
d[10] = (132, 1)
d[11] = (131, 1)
d[12] = (8, 1)
d[14] = (350, 1)
d[15] = (0, 1)
d[16] = (9, 0)
d[17] = (50, 0)
d[18] = (80, 0)
d[19] = (0, 0)
d[20] = ('device return code=0x6985 wrong state', 1)
d[22] = (0, 0)
d[28] = (50, 0)
d[30] = (8, 0)
d[31] = (3300, 0)
d[32] = (2800, 0)
d[34] = (32852, 0)
d[35] = (54741, 0)
d[36] = (300, 0)
d[37] = (100, 0)
d[38] = (1000, 0)
d[39] = (0, 0)
d[40] = (12, 0)
d[41] = (0, 0)
d[42] = (100, 0)
d[128] = ('device return code=0x6a81 function not supported', 0)
d[130] = (6, 0)
d[131] = (65535, 0)
d[132] = (0, 1)
d[133] = (0, 1)
d[134] = (8, 0)
d[135] = (0, 0)
d[137] = (5, 0)
d[138] = (25, 0)
d[139] = (50, 0)
d[140] = (100, 0)
d[141] = (200, 0)
d[142] = (300, 0)
d[143] = (400, 0)
d[144] = (500, 0)
d[145] = (50, 0)
d[146] = (100, 0)
d[147] = (200, 0)
d[148] = (300, 0)
d[149] = (500, 0)
d[150] = (800, 0)
d[151] = (1200, 0)
d[152] = (1500, 0)
d[153] = (0, 0)
d[154] = (0, 0)
d[155] = (34944, 0)
d[156] = (5000, 0)
d[157] = (34832, 0)
d[158] = (34832, 0)
d[159] = (0, 0)
d[160] = (0, 0)
d[161] = (1, 0)
d[162] = (1000, 0)
d[163] = (1, 0)
d[164] = (1000, 0)
d[165] = (2, 0)
d[166] = (1000, 0)
d[167] = (0, 0)
d[168] = (1, 0)
d[169] = (5000, 0)
d[170] = (0, 0)
d[171] = (0, 0)
d[172] = (0, 0)
d[173] = (0, 0)
d[174] = (1, 0)
d[175] = (65535, 0)
d[176] = (0, 0)
d[177] = (0, 0)
d[178] = (0, 0)
d[179] = (0, 0)
d[180] = (0, 0)
d[181] = (65535, 0)
d[183] = (0, 0)
d[184] = (0, 0)
d[185] = (0, 0)
d[186] = (0, 0)
d[187] = (0, 0)
d[188] = (0, 0)
d[189] = (0, 0)
d[190] = (0, 0)
d[191] = (0, 0)
d[192] = (1, 0)
d[193] = (1000, 0)
d[194] = (2, 0)
d[195] = (1000, 0)
d[196] = (0, 0)
d[197] = (1, 0)
d[198] = (1000, 0)
d[199] = (2, 0)
d[200] = (1000, 0)

# Test Rail used parameters
LIST_OF_GET_PARAMS = [0, 6, 7, 10, 11, 12, 14, 16, 17, 18, 19, 23, 30, 33, 40, 128, 130, 131, 132, 133]


# Run get_params
def get_params(config):

    start_suite()
    PORT = config.port
    INIT_MODE = config.init_mode

    FAILED_CASES = 0

    print_info_message("", False, True, False)
    print_info_message('********** Running ' + get_params.__name__ + ' **********', False, True, False)
    # Run init
    ret = test_tool_cmd(PORT, 'initialize', INIT_MODE)
    if ret != 0:
        return ret
    # Run get_param from 0 to 255
    for i in range(0, 256):
    #for i in LIST_OF_GET_PARAMS:
        # If following default outputs exist it is negative case
        expected_outputs = str(d[i][0])
        if (expected_outputs == str('device return code=0x6a80 Bad parameters') or expected_outputs == str('device return code=0x6985 wrong state') or expected_outputs == str('device return code=0x6a81 function not supported-')):  # negative_test is true for outputs with Bad parameters etc.
            is_negative_test = True
        else:
            is_negative_test = False

        ret = test_tool_cmd(PORT, 'get_param', str(i), is_negative_test, expected_outputs)
        if ret != 0:
            FAILED_CASES += 1

    print_info_message("Total failed cases: " + str(FAILED_CASES))
    end_suite()
    return ret