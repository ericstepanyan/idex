from tests.test_rail.Get_Set_and_Store_Restore_params_tests import *
from tests.test_rail.supported_commands_test_suite import *
from tests.test_rail.negative_tests import *
from tests.smoke_tests.Smoke_tests import *
from tests.smoke_tests.new_smoke_test import *


import sys






#Get and set_params test suite
def get_set_and_store_restore_params_test_suite(config, test_name=None):
    if test_name is None:
        passed = 0

        # run get_params

        start_case()
        ret = get_params(config)
        end_case(ret)
        if ret == 0:
            print_successful_message('********** ' + get_params.__name__ + ' has been successfully passed **********', False, True, False)
            passed += 1
        else:
            print_error_message('********** ' + get_params.__name__ + ' failed **********', False, True, False)

        # run set_131

        start_case()
        ret = set_131(config)
        end_case(ret)
        if ret == 0:
            print_successful_message('********** ' + set_131.__name__ + ' has been successfully passed **********', False, True, False)
            passed += 1
        else:
            print_error_message('********** ' + set_131.__name__ + ' failed **********', False, True, False)

        # run set_7

        start_case()
        ret = set_7(config)
        end_case(ret)
        if ret == 0:
            print_successful_message('********** ' + set_7.__name__ + ' has been successfully passed **********', False, True, False)
            passed += 1
        else:
            print_error_message('********** ' + set_7.__name__ + ' failed **********', False, True, False)

        #run set_17

        start_case()
        ret = set_17(config)
        end_case(ret)
        if ret == 0:
            print_successful_message('********** ' + set_17.__name__ + ' has been successfully passed **********', False, True, False)
            passed += 1
        else:
            print_error_message('********** ' + set_17.__name__ + ' failed **********', False, True, False)

        # run set_18

        start_case()
        ret = set_18(config)
        end_case(ret)
        if ret == 0:
            print_successful_message('********** ' + set_18.__name__ + ' has been successfully passed **********', False, True, False)
            passed += 1
        else:
            print_error_message('********** ' + set_18.__name__ + ' failed **********', False, True, False)



        # run store_params_131

        start_case()
        ret = store_params_131(config)
        end_case(ret)
        if ret == 0:
            print_successful_message('********** ' + store_params_131.__name__ + ' has been successfully passed **********', False, True, False)
            passed += 1
        else:
            print_error_message('********** ' + store_params_131.__name__ + ' failed **********', False, True, False)

        # run store_params_7

        start_case()
        ret = store_params_7(config)
        end_case(ret)
        if ret == 0:
            print_successful_message('********** ' + store_params_7.__name__ + ' has been successfully passed **********', False, True, False)
            passed += 1
        else:
            print_error_message('********** ' + store_params_7.__name__ + ' failed **********', False, True, False)

        # run restore_params_7

        start_case()
        ret = restore_params_7(config)
        end_case(ret)
        if ret == 0:
            print_successful_message('********** ' + restore_params_7.__name__ + ' has been successfully passed **********', False, True, False)
            passed += 1
        else:
            print_error_message('********** ' + restore_params_7.__name__ + ' failed **********', False, True, False)

        # run store_params_17

        start_case()
        ret = store_params_17(config)
        end_case(ret)
        if ret == 0:
            print_successful_message('********** ' + store_params_17.__name__ + ' has been successfully passed **********', False, True, False)
            passed += 1
        else:
            print_error_message('********** ' + store_params_17.__name__ + ' failed **********', False, True, False)

        # run restore_params_17

        start_case()
        ret = restore_params_17(config)
        end_case(ret)
        if ret == 0:
            print_successful_message('********** ' + restore_params_17.__name__ + ' has been successfully passed **********', False, True, False)
            passed += 1
        else:
            print_error_message('********** ' + restore_params_17.__name__ + ' failed **********', False, True, False)

        print_info_message("Summary report: " + str(passed) + " from 10 tests have passed")

    else:
        start_case()
        ret = test_name(config)
        end_case(ret)
        if ret == 0:
            print_successful_message('********** ' + test_name.__name__ + ' has been successfully passed **********', False, True, False)
        else:
            print_error_message('********** ' + test_name.__name__ + ' failed **********', False, True, False)



def supported_commands_test_suite(config, test_name=None):
    if test_name is None:
        passed = 0

        # run test_initialize

        start_case()
        ret = test_initialize(config)
        end_case(ret)
        if ret == 0:
            print_successful_message('********** ' + test_initialize.__name__ + ' has been successfully passed **********', False, True, False)
            passed += 1
        else:
            print_error_message('********** ' + test_initialize.__name__ + ' failed **********', False, True, False)

        # run test_initialize_repetitive

        start_case()
        ret = test_initialize_repetitive(config)
        end_case(ret)
        if ret == 0:
            print_successful_message('********** ' + test_initialize_repetitive.__name__ + ' has been successfully passed **********', False, True, False)
            passed += 1
        else:
            print_error_message('********** ' + test_initialize_repetitive.__name__ + ' failed **********', False, True, False)

        # run test_uninitialize

        start_case()
        ret = test_uninitialize(config)
        end_case(ret)
        if ret == 0:
            print_successful_message('********** ' + test_uninitialize.__name__ + ' has been successfully passed **********', False, True, False)
            passed += 1
        else:
            print_error_message('********** ' + test_uninitialize.__name__ + ' failed **********', False, True, False)

        # run get_sensor_fw_version

        start_case()
        ret = get_sensor_fw_version(config)
        end_case(ret)
        if ret == 0:
            print_successful_message('********** ' + get_sensor_fw_version.__name__ + ' has been successfully passed **********', False, True, False)
            passed += 1
        else:
            print_error_message('********** ' + get_sensor_fw_version.__name__ + ' failed **********', False, True, False)

        # run get_version

        start_case()
        ret = get_version(config)
        end_case(ret)
        if ret == 0:
            print_successful_message('********** ' + get_version.__name__ + ' has been successfully passed **********', False, True, False)
            passed += 1
        else:
            print_error_message('********** ' + get_version.__name__ + ' failed **********', False, True, False)

        # run get_uid

        start_case()
        ret = get_uid(config)
        end_case(ret)
        if ret == 0:
            print_successful_message('********** ' + get_uid.__name__ + ' has been successfully passed **********', False, True, False)
            passed += 1
        else:
            print_error_message('********** ' + get_uid.__name__ + ' failed **********', False, True, False)

        # run test_calibrate

        start_case()
        ret = test_calibrate(config)
        end_case(ret)
        if ret == 0:
            print_successful_message('********** ' + test_calibrate.__name__ + ' has been successfully passed **********', False, True, False)
            passed += 1
        else:
            print_error_message('********** ' + test_calibrate.__name__ + ' failed **********', False, True, False)

        # run test_calibrate_force

        start_case()
        ret = test_calibrate_force(config)
        end_case(ret)
        if ret == 0:
            print_successful_message('********** ' + test_calibrate_force.__name__ + ' has been successfully passed **********', False, True, False)
            passed += 1
        else:
            print_error_message('********** ' + test_calibrate_force.__name__ + ' failed **********', False, True, False)

        print_info_message("Summary report: " + str(passed) + " from 8 tests have passed")

    else:
        start_case()
        ret = test_name(config)
        end_case(ret)
        if ret == 0:
            print_successful_message('********** ' + test_name.__name__ + ' has been successfully passed **********', False, True, False)
        else:
            print_error_message('********** ' + test_name.__name__ + ' failed **********', False, True, False)




def negative_test_suite(config, test_name=None):
    if test_name is None:
        passed = 0

        # run get_param_99

        start_case()
        ret = get_param_99(config)
        end_case(ret)
        if ret == 0:
            print_successful_message('********** ' + get_param_99.__name__ + ' has been successfully passed **********', False, True, False)
            passed += 1
        else:
            print_error_message('********** ' + get_param_99.__name__ + ' failed **********', False, True, False)

        # run set_param_99_0

        start_case()
        ret = set_param_99_0(config)
        end_case(ret)
        if ret == 0:
            print_successful_message('********** ' + set_param_99_0.__name__ + ' has been successfully passed **********', False, True, False)
            passed += 1
        else:
            print_error_message('********** ' + set_param_99_0.__name__ + ' failed **********', False, True, False)

        print_info_message("Summary report: " + str(passed) + " from 8 tests have passed")

        # run set_param_10_35

        start_case()
        ret = set_param_10_35(config)
        end_case(ret)
        if ret == 0:
            print_successful_message('********** ' + set_param_10_35.__name__ + ' has been successfully passed **********', False, True, False)
            passed += 1
        else:
            print_error_message('********** ' + set_param_10_35.__name__ + ' failed **********', False, True, False)

        # run set_param_30

        start_case()
        ret = set_param_30(config)
        end_case(ret)
        if ret == 0:
            print_successful_message('********** ' + set_param_30.__name__ + ' has been successfully passed **********', False, True, False)
            passed += 1
        else:
            print_error_message('********** ' + set_param_30.__name__ + ' failed **********', False, True, False)

        # run invalid_sequence_get_template_without_acquire

        start_case()
        ret = invalid_sequence_get_template_without_acquire(config)
        end_case(ret)
        if ret == 0:
            print_successful_message('********** ' + invalid_sequence_get_template_without_acquire.__name__ + ' has been successfully passed **********', False, True, False)
            passed += 1
        else:
            print_error_message('********** ' + invalid_sequence_get_template_without_acquire.__name__ + ' failed **********', False, True, False)

        # run invalid_sequence_acquire_without_initialize

        start_case()
        ret = invalid_sequence_acquire_without_initialize(config)
        end_case(ret)
        if ret == 0:
            print_successful_message('********** ' + invalid_sequence_acquire_without_initialize.__name__ + ' has been successfully passed **********', False, True, False)
            passed += 1
        else:
            print_error_message('********** ' + invalid_sequence_acquire_without_initialize.__name__ + ' failed **********', False, True, False)

        # run get_param_25

        start_case()
        ret = get_param_25(config)
        end_case(ret)
        if ret == 0:
            print_successful_message('********** ' + get_param_25.__name__ + ' has been successfully passed **********', False, True, False)
            passed += 1
        else:
            print_error_message('********** ' + get_param_25.__name__ + ' failed **********', False, True, False)

        # run unsupported_command_template

        start_case()
        ret = unsupported_command_template(config)
        end_case(ret)
        if ret == 0:
            print_successful_message('********** ' + unsupported_command_template.__name__ + ' has been successfully passed **********', False, True, False)
            passed += 1
        else:
            print_error_message('********** ' + unsupported_command_template.__name__ + ' failed **********', False, True, False)

        # run unsupported_command_details

        start_case()
        ret = unsupported_command_details(config)
        end_case(ret)
        if ret == 0:
            print_successful_message('********** ' + unsupported_command_details.__name__ + ' has been successfully passed **********', False, True, False)
            passed += 1
        else:
            print_error_message('********** ' + unsupported_command_details.__name__ + ' failed **********', False, True, False)

        # run unsupported_command_key

        start_case()
        ret = unsupported_command_key(config)
        end_case(ret)
        if ret == 0:
            print_successful_message('********** ' + unsupported_command_key.__name__ + ' has been successfully passed **********', False, True, False)
            passed += 1
        else:
            print_error_message('********** ' + unsupported_command_key.__name__ + ' failed **********', False, True, False)

        # run unsupported_command_format

        start_case()
        ret = unsupported_command_format(config)
        end_case(ret)
        if ret == 0:
            print_successful_message('********** ' + unsupported_command_format.__name__ + ' has been successfully passed **********', False, True, False)
            passed += 1
        else:
            print_error_message('********** ' + unsupported_command_format.__name__ + ' failed **********', False, True, False)

        # run unsupported_command_fat

        start_case()
        ret = unsupported_command_fat(config)
        end_case(ret)
        if ret == 0:
            print_successful_message('********** ' + unsupported_command_fat.__name__ + ' has been successfully passed **********', False, True, False)
            passed += 1
        else:
            print_error_message('********** ' + unsupported_command_fat.__name__ + ' failed **********', False, True, False)

        # run unsupported_command_flash

        start_case()
        ret = unsupported_command_flash(config)
        end_case(ret)
        if ret == 0:
            print_successful_message('********** ' + unsupported_command_flash.__name__ + ' has been successfully passed **********', False, True, False)
            passed += 1
        else:
            print_error_message('********** ' + unsupported_command_flash.__name__ + ' failed **********', False, True, False)

        print_info_message("Summary report: " + str(passed) + " from 13 tests have passed")

    else:
        start_case()
        ret = test_name(config)
        end_case(ret)
        if ret == 0:
            print_successful_message('********** ' + test_name.__name__ + ' has been successfully passed **********', False, True, False)
        else:
            print_error_message('********** ' + test_name.__name__ + ' failed **********', False, True, False)








def new_smoke_test_suite(config, test_name=None):
    if test_name is None:
        passed = 0

        # run one_touch_Enrol_and_Match_using_loaded_images

        start_case()
        ret = one_touch_Enrol_and_Match_using_loaded_images(config)
        end_case(ret)
        if ret == 0:
            print_successful_message('********** ' + one_touch_Enrol_and_Match_using_loaded_images.__name__ + ' has been successfully passed **********', False, True, False)
            passed += 1
        else:
            print_error_message('********** ' + one_touch_Enrol_and_Match_using_loaded_images.__name__ + ' failed **********', False, True, False)

        # run one_touch_Enrol_and_Match_using_live_images

        start_case()
        ret = one_touch_Enrol_and_Match_using_live_images(config)
        end_case(ret)
        if ret == 0:
            print_successful_message('********** ' + one_touch_Enrol_and_Match_using_live_images.__name__ + ' has been successfully passed **********', False, True, False)
            passed += 1
        else:
            print_error_message('********** ' + one_touch_Enrol_and_Match_using_live_images.__name__ + ' failed **********', False, True, False)

        # run enrol_and_Match_2_fingers_6_touches

        start_case()
        ret = enrol_and_Match_2_fingers_6_touches(config)
        end_case(ret)
        if ret == 0:
            print_successful_message('********** ' + enrol_and_Match_2_fingers_6_touches.__name__ + ' has been successfully passed **********', False, True, False)
            passed += 1
        else:
            print_error_message('********** ' + enrol_and_Match_2_fingers_6_touches.__name__ + ' failed **********', False, True, False)

        # run enrol_and_match_over_non_secure_channel

        start_case()
        ret = enrol_and_match_over_non_secure_channel(config)
        end_case(ret)
        if ret == 0:
            print_successful_message('********** ' + enrol_and_match_over_non_secure_channel.__name__ + ' has been successfully passed **********', False, True, False)
            passed += 1
        else:
            print_error_message('********** ' + enrol_and_match_over_non_secure_channel.__name__ + ' failed **********', False, True, False)


        # run enrol_pattern

        start_case()
        ret = enrol_pattern(config)
        end_case(ret)
        if ret == 0:
            print_successful_message('********** ' + enrol_pattern.__name__ + ' has been successfully passed **********', False, True, False)
            passed += 1
        else:
            print_error_message('********** ' + enrol_pattern.__name__ + ' failed **********', False, True, False)

        # run delete_enrolled_fingers

        start_case()
        ret = delete_enrolled_fingers(config)
        end_case(ret)
        if ret == 0:
            print_successful_message('********** ' + delete_enrolled_fingers.__name__ + ' has been successfully passed **********', False, True, False)
            passed += 1
        else:
            print_error_message('********** ' + delete_enrolled_fingers.__name__ + ' failed **********', False, True, False)


        # run get_and_set_parameters

        start_case()
        ret = get_and_set_parameters(config)
        end_case(ret)
        if ret == 0:
            print_successful_message('********** ' + get_and_set_parameters.__name__ + ' has been successfully passed **********', False, True, False)
            passed += 1
        else:
            print_error_message('********** ' + get_and_set_parameters.__name__ + ' failed **********', False, True, False)

        # run dm_match_timestamp

        start_case()
        ret = dm_match_timestamp(config)
        end_case(ret)
        if ret == 0:
            print_successful_message('********** ' + dm_match_timestamp.__name__ + ' has been successfully passed **********', False, True, False)
            passed += 1
        else:
            print_error_message('********** ' + dm_match_timestamp.__name__ + ' failed **********', False, True, False)

        # run mwf_function

        start_case()
        ret = mwf_function(config)
        end_case(ret)
        if ret == 0:
            print_successful_message('********** ' + mwf_function.__name__ + ' has been successfully passed **********', False, True, False)
            passed += 1
        else:
            print_error_message('********** ' + mwf_function.__name__ + ' failed **********', False, True, False)

        # run version_reporting

        start_case()
        ret = version_reporting(config)
        end_case(ret)
        if ret == 0:
            print_successful_message('********** ' + version_reporting.__name__ + ' has been successfully passed **********', False, True, False)
            passed += 1
        else:
            print_error_message('********** ' + version_reporting.__name__ + ' failed **********', False, True, False)

        # run secure_channel_SE_enable

        start_case()
        ret = secure_channel_SE_enable(config)
        end_case(ret)
        if ret == 0:
            print_successful_message('********** ' + secure_channel_SE_enable.__name__ + ' has been successfully passed **********', False, True, False)
            passed += 1
        else:
            print_error_message('********** ' + secure_channel_SE_enable.__name__ + ' failed **********', False, True, False)

        # run enrol_and_match_over_secure_channel

        start_case()
        ret = enrol_and_match_over_secure_channel(config)
        end_case(ret)
        if ret == 0:
            print_successful_message(
                '********** ' + enrol_and_match_over_secure_channel.__name__ + ' has been successfully passed **********', False, True, False)
            passed += 1
        else:
            print_error_message('********** ' + enrol_and_match_over_secure_channel.__name__ + ' failed **********', False, True, False)

        # run get_and_set_parameters_over_secure_channel

        start_case()
        ret = get_and_set_parameters_over_secure_channel(config)
        end_case(ret)
        if ret == 0:
            print_successful_message(
                '********** ' + get_and_set_parameters_over_secure_channel.__name__ + ' has been successfully passed **********', False, True, False)
            passed += 1
        else:
            print_error_message('********** ' + get_and_set_parameters_over_secure_channel.__name__ + ' failed **********', False, True, False)



        print_info_message("Summary report: " + str(passed) + " from 13 tests have passed")

    else:
        start_case()
        ret = test_name(config)
        end_case(ret)
        if ret == 0:
            print_successful_message('********** ' + test_name.__name__ + ' has been successfully passed **********', False, True, False)
        else:
            print_error_message('********** ' + test_name.__name__ + ' failed **********', False, True, False)







def main():
    ret = 1
    global config
    config = appConfig()
    if len(sys.argv) == 3:
        config = initialize(sys.argv[1], sys.argv[2])
    elif len(sys.argv) == 2:
        config = initialize(sys.argv[1], '')
    else:
        print_error_message("Communication mode and test tool are not provided")
        print_info_message("Try Run_Tests.py --help or -h for more information\nUSAGE EXAMPLE: Run_Tests.py contactless idex-test", False, True, False)
        exit(1)
    try:
        ret = eval(data["Test suite"])
    except NameError:
        print_error_message("Wrong test suite name is provided in config file")

    uninitialize()
    exit(ret)


if __name__ == '__main__':
    main()
