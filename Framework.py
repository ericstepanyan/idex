import subprocess
import time
import os
import enum
import pathlib
from shutil import which
import ctypes
import json
os.system('')

# Script path
script_path = str(pathlib.Path(__file__).parent.absolute())

# Opens config.json file and parses it assigning to data variable

with open(script_path + "\\" + "config.json", "r") as conf_file:
    data = json.load(conf_file)



# Variables

idex_test_directory_path = data["idex_test_directory_path"]

script_tools_dirname = "tools"

ykush_dirname = "ykush"

ykush_path = script_path + "\\" + script_tools_dirname + "\\" + ykush_dirname + "\\"

script_database_dirname = "database"

script_set_param_131_dirname = "set_param_131_case"

script_4patterns_case_dirname = "4patterns_case"

script_4patterns_case_images_dirname = "images"

script_one_touch_match_loaded_case_dirname = "one_touch_match_loaded_case"

script_one_touch_match_loaded_case_images_dirname = "images"

COMMUNICATION_MODE = ''

APP = ''

POWER_SWITCH_APP = 'ykushcmd'

CHUNKS_ON = ''

CHUNKS_OFF = ''

PORT = ''

INIT_MODE = ''

IDX_SERIAL_TEST_CMD_HEADER = ''

IDEX_TEST_ERROR_WORDS = ['ERROR', 'RESPONSE']

POWER_SWITCH_APP_PORT = 1

phrase = 'No YKUSH boards found.'

OUTPUT_LOG_FILENAME = script_path + "\\" + 'output_log.txt'

log_file = 0

ERROR_LOG_FILENAME = script_path + "\\" + 'error_log.txt'

error_file = 0

SCRIPT_TEST_TOOL = ''
SCRIPT_COMMUNICATION = ''

buffer = ''




# Define classes

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

class appConfig:
    def __init__(self):
        self.port = 0
        self.init_mode = 0

class build_mode(enum.Enum):
    RELEASE = 1
    DEBUG = 2

class script_test_tool(enum.Enum):
    IDEX_TEST = 1
    IDEX_SERIAL_TEST = 2

class communication_mode(enum.Enum):
   COM_MOCK = 1
   APDU_CONTACT = 2
   APDU_CONTACTLESS = 3






#BUILD_MODE = build_mode.RELEASE
BUILD_MODE = build_mode.DEBUG








# string: message to be printed
# is_debug_msg: required argument, defines if it is debug or not
# is_new_line: optional argument, defines if message is printed on same line or new line
# is_header_printed: optional argument, defines if header is printed or not

def print_info_message(string, is_debug_msg=False, is_new_line=True, is_header_printed=True):
    global buffer
    if not (BUILD_MODE == build_mode.RELEASE and is_debug_msg == True):
        if is_header_printed == False:
            header = ""
        else:
            if is_debug_msg == True:
                header = "DBG_INFO: "
            else:
                header = "INFO: "

        msg = header + string
        buffer += msg

        if is_new_line == False:
            print(msg, end="")
        else:
            print(msg)
            buffer += "\n"

def print_successful_message(string, is_debug_msg=False, is_new_line=True, is_header_printed=True):
    global buffer
    if not (BUILD_MODE == build_mode.RELEASE and is_debug_msg == True):
        if is_header_printed == False:
            header = ""
        else:
            if is_debug_msg == True:
                header = "DBG_SUCCESSFUL: "
            else:
                header = "SUCCESSFUL: "

        msg = header + string
        print_msg = bcolors.OKGREEN + msg + bcolors.ENDC

        buffer += msg

        if is_new_line == False:
            print(print_msg, end="")
        else:
            print(print_msg)
            buffer += "\n"

def print_warning_message(string, is_debug_msg=False, is_new_line=True, is_header_printed=True):
    global buffer
    if not (BUILD_MODE == "RELEASE" and is_debug_msg == True):
        if is_header_printed == False:
            header = ""
        else:
            if is_debug_msg == True:
                header = "DBG_WARNING: "
            else:
                header = "WARNING "

        msg = header + string
        print_msg = bcolors.WARNING + msg + bcolors.ENDC

        buffer += msg

        if is_new_line == False:
            print(print_msg, end="")
        else:
            print(print_msg)
            buffer += "\n"

def print_error_message(string, is_debug_msg=False, is_new_line=True, is_header_printed=True):
    global buffer
    if not (BUILD_MODE == "RELEASE" and is_debug_msg == True):
        if is_header_printed == False:
            header = ""
        else:
            if is_debug_msg == True:
                header = "DBG_ERROR: "
            else:
                header = "ERROR "

        msg = header + string
        print_msg = bcolors.FAIL + msg + bcolors.ENDC

        buffer += msg

        if is_new_line == False:
            print(print_msg, end="")
        else:
            print(print_msg)
            buffer += "\n"

# Config function setting required parameters: APP and PORT
def config():
    global APP
    global COMMUNICATION_MODE
    global CHUNKS_OFF
    global CHUNKS_ON
    global PORT
    global INIT_MODE

    if SCRIPT_COMMUNICATION == communication_mode.COM_MOCK:
        COMMUNICATION_MODE = 'C'
        #PORT = ''
        if SCRIPT_TEST_TOOL == script_test_tool.IDEX_TEST:
            APP = 'idex-test'
            PORT = ''
        elif SCRIPT_TEST_TOOL == script_test_tool.IDEX_SERIAL_TEST:
            APP = 'idx-serial-test'
            PORT = ''
            IDX_SERIAL_TEST_CMD_HEADER = '--test'

    if SCRIPT_COMMUNICATION == communication_mode.APDU_CONTACT:
        COMMUNICATION_MODE = 'C'
        if SCRIPT_TEST_TOOL == script_test_tool.IDEX_TEST:
            APP = 'idex-test'
            PORT = '--port PCSC-'+ COMMUNICATION_MODE
            CHUNKS_ON = '--chunks_on'
            CHUNKS_OFF = '--chunks_off'
        elif SCRIPT_TEST_TOOL == script_test_tool.IDEX_SERIAL_TEST:
            APP = 'idx-serial-test'
            PORT = '--port PCSC-'+ COMMUNICATION_MODE #TODO, check if this works


    if SCRIPT_COMMUNICATION == communication_mode.APDU_CONTACTLESS:
        COMMUNICATION_MODE = 'CL'
        if SCRIPT_TEST_TOOL == script_test_tool.IDEX_TEST:
            APP = 'idex-test'
            PORT = '--port PCSC-'+ COMMUNICATION_MODE
            CHUNKS_ON = '--chunks_on'
            CHUNKS_OFF = '--chunks_off'
        elif SCRIPT_TEST_TOOL == script_test_tool.IDEX_SERIAL_TEST:
            APP = 'idx-serial-test'
            PORT = '--port PCSC-'+ COMMUNICATION_MODE #TODO, check if this works



    if COMMUNICATION_MODE == 'CL':
        INIT_MODE = ' contactless'
    else:
        INIT_MODE = ' contact'


# checking required tools

def check_required_tools():
    print_info_message("Checking " + APP + " existence ... ", True, False, True)
    if which(APP) is not None:
        print_successful_message("DONE", True, True, False)
    elif which(APP) is None:
        print_error_message('\"' + APP + '\"' + " application doesn't exist", False, True, True)
        exit(1)
    print_info_message("Checking " + POWER_SWITCH_APP + " existence ... ", True, False, True)
    if which(POWER_SWITCH_APP) is not None:
        ykush_check_process = subprocess.run(ykush_path + "ykushcmd.exe -l", text=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True)
        if phrase in ykush_check_process.stdout:
            print_error_message("No YKUSH boards found.")
            print_warning_message("manual power off/on will be requested", False, True, True)
        else:
            print_successful_message("DONE", True, True, False)

    if which(POWER_SWITCH_APP) is None:
        print_warning_message('\"' + POWER_SWITCH_APP + '\"' + " application doesn't exist, manual power off/on will be requested", False, True, True)
    return 0




# Checking dependant apps & tools and return 0 if succeeded
def check_requirements():
    print_info_message("Checking requirements", True, True, True)
    return check_required_tools()


# Initialize function: return 0 if succeeded
def initialize(comm_mode, test_tool):
    global SCRIPT_COMMUNICATION
    global SCRIPT_TEST_TOOL
    global PORT
    global INIT_MODE


# Check the system argument: if user provided right communication mode and test tool
    if comm_mode == '-h' or comm_mode == '--help' and test_tool == '':
        print_info_message("\n***SYNOPSIS***\n\n\tThis is an AUTOMATED TEST RUN FRAMEWORK\n", False, True, False)
        print_info_message("***DESCRIPTION***\n\n\tUser needs to open config.json file located in current directory and modify using following guide\n\n"
                           "\t1.Insert path to idex test directory\n\t2.Insert all corresponding version fields\n\t3.Sensor info field may be changed depending on sensor type (Polaris/Aurora)", False, True, False)
        print_info_message("\t4.Insert required test suite for auto run\n\t5.Insert secure element name to be used for test\t(options: THD/SLE/MOCK)\n\nAvailable test suites list:\n\t- supported_commands_test_suite(config), \n\t- get_set_and_store_restore_params_test_suite(config), "
                           "\n\t- negative_test_suite(config), \n\t- new_smoke_test_suite(config)\n", False, True, False)
        print_info_message("If particular test needs to be run from test suite, insert name of test as second argument:\tExample: new_smoke_tests_suite(config, version_reporting)\n", False, True, False)
        print_info_message("***PARAMETER***\n\n\tRun_Tests.py script must receive 2 parameters: Communication mode and Test tool\n", False, True, False)
        print_info_message("\tCommunication modes:\n\t\t mock/contact/contactless\n\n\tTest tools:\n\t\t idex-test/idx-serial-test\n",False, True, False)
        print_info_message("***EXAMPLE***\n\n\tRun_Tests.py contactless idex-test", False, True, False)
        exit(1)
    if comm_mode == 'contactless':
        SCRIPT_COMMUNICATION = communication_mode.APDU_CONTACTLESS
    elif comm_mode == 'contact':
        SCRIPT_COMMUNICATION = communication_mode.APDU_CONTACT
    elif comm_mode == 'mock':
        SCRIPT_COMMUNICATION = communication_mode.COM_MOCK
    else:
        print_error_message("WRONG COMMUNICATION MODE")
        print_info_message("Choose one of the following communication modes: mock, contact, contactless")
        print_info_message("USAGE EXAMPLE: Run_Tests.py contactless idex-test", False, True, False)
        exit(1)
    if test_tool == 'idex-test':
        SCRIPT_TEST_TOOL = script_test_tool.IDEX_TEST
    elif test_tool == 'idx-serial-test':
        SCRIPT_TEST_TOOL = script_test_tool.IDEX_SERIAL_TEST
    else:
        print_error_message("WRONG TEST TOOL")
        print_info_message("Choose one of the following test tools: idex-test, idx-serial-test")
        print_info_message("USAGE EXAMPLE: Run_Tests.py contactless idex-test", False, True, False)
        exit(1)

# Run config function
    config()

    # Changing directory to desired one
    os.chdir(idex_test_directory_path)
    # Adding ykush path to env variable PATH
    os.environ["PATH"] += os.pathsep + ykush_path


    open_file()
    check_requirements()

    aHandle = appConfig()
    aHandle.port = PORT
    aHandle.init_mode = INIT_MODE
    return aHandle



# Uninitialize function: return 0 if succeeded

def uninitialize():
    return close_file()



# Close file function: return 0 if succeeded

def close_file():
    global log_file
    global error_file
    if log_file.close() and error_file.close():
        return 0
    else:
        return 1


# Open file function: return 0 if succeeded
def open_file():
    global log_file
    global error_file
    log_file = open(OUTPUT_LOG_FILENAME, 'w')
    error_file = open(ERROR_LOG_FILENAME, 'w')
    if log_file and error_file:
        return 0
    else:
        print_error_message('Unable to open file', True)
        return 1


# auto or manual power cycle function
def auto_or_manual_power_cycle():
    ykush_check_process = subprocess.run(ykush_path + "ykushcmd.exe -l", text=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True)
    if which(POWER_SWITCH_APP) is None or phrase in ykush_check_process.stdout:
        return manual_power_cycle()
    else:
        return power_cycle()

# manual power cycle
def manual_power_cycle():
    return input("Turn off the board power and then turn it on and hit ENTER: ")

# power cycle function
def power_cycle():
    power_off()
    time_out_sec(2)
    power_on()
    time_out_sec(4)



# power on the board
def power_on():
    process, string = sys_commands(ykush_path + 'ykushcmd.exe -u ' + str(POWER_SWITCH_APP_PORT))
    if process.returncode == 0:
        print_successful_message('Turning power on', False, True, False)
        return 0
    else:
        print_error_message('Failed to turn power on', False, True, True)
        return 1


# power off the board
def power_off():
    process, string = sys_commands(ykush_path + 'ykushcmd.exe -d ' + str(POWER_SWITCH_APP_PORT))
    if process.returncode == 0:
        print_successful_message('Turning power down', False, True, False)
        return 0
    else:
        print_error_message('Failed to turn power down', False, True, True)
        return 1


# time out function
def time_out_sec(sec):
    time.sleep(sec)




# Running process and returning 0 if succeeded
def sys_commands(string):
    #global buffer
    process = subprocess.run(string, text=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    #log_file.write(">" + string + '\n')
    #log_file.write(process.stdout + '\n')
    #buffer += '\n' + ">" + string + '\n'
    #buffer += '\n' + process.stdout + '\n'
    #log_file.flush()
    return process, string



#     \return 0 if succeeded anything else if not succeeded
def idx_serial_test_cmd(flag, command, options):
    ret = 0
    process, string = sys_commands(APP + ' ' + flag + ' ' + IDX_SERIAL_TEST_CMD_HEADER + ' ' + command + ' ' + options)
    return ret, process, string




# Search for words ERROR and RESPONSE in list and return 0 if succeeded
def idex_test_stdout_search_for_error(std_out):
    for i in IDEX_TEST_ERROR_WORDS:
        if i in std_out:
            return 1
    return 0


# Function to execute idex_test commands
# Return return code from sys_command function
# Check if return code is 0, then pass sys_command function output to idex_test_stdout_search_for_error
# Return return code from idex_test_stdout_search_for_error
def idex_test_cmd(flag, command, options):
    global APP
    process, string = sys_commands(APP + ' ' + flag + ' ' + '--' + command + ' ' + options)
    ret = process.returncode
    if ret == 0:
        ret = idex_test_stdout_search_for_error(process.stdout)
    return ret, process, string


# Function for using either idex_test_cmd or idx_serial_test_cmd
# is_negative_test argument is False by default
# expected_outputs is the list of strings passed to function to check expected output

def test_tool_cmd(flag, command, options, is_negative_test=False, expected_outputs=None):
    global buffer
    string =''

    if expected_outputs is None:
        expected_outputs = []
    ret = 1
    process = 0
    print_info_message('Running ' + "'" + command + " " + options + "'" + ' command...', False, False, True)
    if SCRIPT_TEST_TOOL == script_test_tool.IDEX_TEST:
        ret, process, string = idex_test_cmd(flag, command, options)
    elif SCRIPT_TEST_TOOL == script_test_tool.IDEX_SERIAL_TEST:
        ret, process, string = idx_serial_test_cmd(flag, command, options)
    else:
        print_error_message('Wrong tool is provided', False, True, True)

    # we expect non 0 ret for negative cases
    if is_negative_test is True:
        if ret != 0:
            ret = 0
        else:
            ret = 1

    if ret == 0 and expected_outputs:
        for expected_output in expected_outputs:
            if not expected_output in process.stdout:
                ret = 1
                break
    if ret == 0:
        if expected_outputs:
          print_successful_message(' DONE', False, False, False)
          print_info_message(" (found " + str(expected_outputs) + ")", False, True, False)
        else:
          print_successful_message(' DONE', False, True, False)
    else:
        if expected_outputs:
            global buffer
            #global error_file
            print_error_message(' FAILED ', False, False, False)
            print_info_message(" (not found " + str(expected_outputs) + ")", False, True, False)

            buffer += str(process.args) + "\n" + process.stdout
            #error_file.write(buffer)
        else:
          print_error_message(' FAILED ', False, True, False)
          
        if is_negative_test is True:
            print_warning_message("Negative result expected", False, True, True)

        # To catch windows exceptions (e.g dll is missing)
        if process.returncode < ctypes.c_uint(-1).value and process.returncode > 1:
            print_error_message("Potential Windows exception, Returncode is : " + str(process.returncode), False, True, True)

        print_info_message(process.stdout, False, True, False)
    # Input and process stdout is added in buffer
    buffer += ">" + string + '\n'
    buffer += process.stdout + '\n'

    return ret
# Function is called before test case and opens empty buffer
def start_case():
    global buffer
    buffer = ''
# If buffer is not empty it is written in log file
def end_case(ret):
    if buffer != '':
        global log_file
        log_file.write(buffer)
    # If process return code is not 0 buffer is written in error_log file
    if ret != 0:
        global error_file
        if buffer != '':
            # print(buffer)
            error_file.write(buffer)
